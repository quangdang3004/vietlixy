<?php
namespace backend\controllers;

use backend\models\Banner;
use backend\models\ImagesBanner;
use backend\models\QuanLyBanner;
use backend\models\QuaTang;
use common\models\myAPI;
use common\models\User;
use Faker\Provider\Image;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class QuaTangController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-all-data', 'load', 'save', 'delete', 'xoa-anh-qua-tang'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('QuaTang', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }
    // get-all-data
    public function actionGetAllData(){
        $query = QuaTang::find();
        $totalCount = $query->count();

        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) > 0) {
                $arrFieldSearch = ['name', 'gia_tri'];
                foreach ($arrFieldSearch as $item) {
                    if ((isset($this->dataPost['fieldsSearch']['value'][$item])))
                        if ($this->dataPost['fieldsSearch']['value'][$item] != '')
                            $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                }
            }
        }
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->orderBy(['id' => SORT_DESC])
            ->andFilterWhere(['active' => 1])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** load */
    public function actionLoad(){
        /** @var QuaTang $user */
        $user = QuaTang::find()
            ->andFilterWhere(['id' => $this->dataPost['danh_muc']])
            ->andFilterWhere(['active' => 1])
            ->one();
        if(!is_null($user)){
            return [
                'banner' => $user,
            ];
        }
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    /** save */
    public function actionSave(){
        $oldImage = null;
        if($this->dataPost['id'] != ''){
            $banner = QuaTang::findOne($this->dataPost['id']);
            $banner->updated = date("Y-m-d H:i:s");
            $banner->user_updated_id = $this->dataPost['uid'];
            if($banner->anh_dai_dien != 'no-image.jpg')
                $oldImage = dirname(dirname(__DIR__)).'/images/'.$banner->anh_dai_dien;
        }
        else{
            $banner = new QuaTang();
            $banner->created = date("Y-m-d H:i:s");
            $banner->user_id = $this->dataPost['uid'];
        }
        $banner->name = $this->dataPost['name'];
        $banner->gia_tri = intval(str_replace(',','', $this->dataPost['gia_tri']));

        if($banner->save()){

            if (!is_null($this->dataPost['anh_dai_dien']) && $this->dataPost['anh_dai_dien'] != ''){
                $arr_data_image = explode(',', $this->dataPost['anh_dai_dien']);
                $file = base64_decode($arr_data_image[1]);
                $loai_file = explode('/', explode(';', $arr_data_image[0])[0])[1];
                $fileName = time().myAPI::createCode($this->dataPost['name']).'.'.$loai_file;

                file_put_contents(dirname(dirname(__DIR__)).'/images/'.$fileName, $file);
                $banner->updateAttributes(['anh_dai_dien' => $fileName]);

                if(!is_null($oldImage))
                    unlink($oldImage);  //Xóa ảnh cũ
            }

            return [
                'content' => 'Đã lưu thông tin quà tặng'
            ];
        }
        else
            throw new HttpException(500, Html::errorSummary($banner));
    }

    /** delete */
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        QuaTang::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'content' => 'Đã xóa dữ liệu thành công',
        ];
    }

    //xoa-anh-qua-tang
    public function actionXoaAnhQuaTang(){
        if(isset($this->dataPost['hinhAnh'])){
            /** @var QuaTang $model */
            $model = QuaTang::find()
                ->andFilterWhere(['id' => $this->dataPost['hinhAnh']])
                ->andFilterWhere(['active' => 1])
                ->one();
            if(is_null($model))
                throw new HttpException(500, "Thông tin không hợp lệ");
            else{
                if($model->anh_dai_dien != 'no-image.jpg'){
                    $path = dirname(dirname(__DIR__)).'/images/'.$model->anh_dai_dien;
                    if(is_file($path)){
                        $model->updateAttributes(['anh_dai_dien' => 'no-image.jpg']);
                        unlink($path);
                    }
                }
            }
        }
        return [
            'content' => 'Đã xóa hình ảnh thành công',
        ];
    }
}
