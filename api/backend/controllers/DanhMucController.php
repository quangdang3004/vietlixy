<?php

namespace backend\controllers;

use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class DanhMucController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-all-khu-vuc', 'get-data', 'load', 'save', 'delete', 'get-list-data'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('DanhMuc', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    // get-all-khu-vuc
    public function actionGetAllKhuVuc(){
        $query = DanhMuc::findAll(['active' => 1, 'type' => DanhMuc::KHU_VUC]);
        $data = [];
        foreach ($query as $item) {
            $data[] = ['label' => $item->name, 'value' => $item->id];
        }
        return [
            'khuVuc' => $data
        ];
    }

    // get-data
    public function actionGetData(){
        $query = DanhMuc::find()->andFilterWhere(['active' => 1]);
        if (isset($this->dataPost['fieldsSearch'])){
            if (count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['name', 'type'];
                foreach ($arrFieldSearch as $item){
                    if (trim($item) != '') {
                        $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                    }
                }
            }
        }
        $totalCount = $query->count();
        $data = $query
            ->select(['id', 'name', 'type', 'code', 'active', 'ghi_chu', 'image', 'ti_le_hoan_tien', 'noi_bat', 'vi_tri', 'type_hoan_tien', 'link'])
            ->orderBy(['id'=> SORT_DESC])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** load */
    public function actionLoad(){
        $user = DanhMuc::find()->andFilterWhere(['id' => $this->dataPost['danh_muc'], 'active' => 1])
            ->one();
        $user->type = ['value' => $user->type, 'label' => $user->type];
        $user->noi_bat = $user->noi_bat == 1;
        if(!is_null($user))
            return  $user;
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new DanhMuc();
        else
            $model = DanhMuc::findOne($this->dataPost['id']);

        $arrSave = ['name', 'code', 'link', 'image'];
        foreach ($arrSave as $item){
            if (isset($this->dataPost[$item])){
                if ($this->dataPost[$item] != ''){
                    $model->code = $this->dataPost[$item];
                }
            }
        }
        $model->type = $this->dataPost['type']['value'];
        $model->ti_le_hoan_tien = doubleval(str_replace(',', '', $this->dataPost['ti_le_hoan_tien']));
        if (isset($this->dataPost['type_hoan_tien']['value']))
            $model->type_hoan_tien = $this->dataPost['type_hoan_tien']['value'];
//        $model->vi_tri = doubleval(str_replace(',', '', $this->dataPost['vi_tri']));
        if($model->save())
            return [
                'content' => 'Cập nhật thông tin danh mục thành công'
            ];
        throw new HttpException(500, Html::errorSummary($model));
    }

    /** delete */
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        DanhMuc::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'message' => 'Xóa dữ liệu thành công',
        ];
    }

    // get-list-data
    public function actionGetListData(){
        $data = DanhMuc::findAll(['type' => $this->dataPost['type']]);
        $results = [];
        foreach ($data as $item) {
            $results[] = ['key' => $item->id, 'label' => $item->name];
        }

        return [
            'results' => $results,
        ];
    }

}
