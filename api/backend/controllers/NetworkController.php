<?php

namespace backend\controllers;

use backend\models\ChienDich;
use backend\models\Network;
use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class NetworkController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-data', 'load', 'save', 'delete', 'get-network', 'get-chien-dich'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('Network', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    // get-data
    public function actionGetData(){
        $query = Network::find();
        if(isset($this->dataPost['fieldsSearch'])){
            if(count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['Name', 'Subid', 'Order'];
                foreach ($arrFieldSearch as $item) {
                    if(trim($item)!='')
                        $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                }
            }
        }
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->andFilterWhere(['active' => 1])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** load */
    public function actionLoad(){
        $user = Network::find()->andFilterWhere(['id' => $this->dataPost['danh_muc']])
            ->one();
        if(!is_null($user))
            return $user;
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new Network();
        else
            $model = Network::findOne($this->dataPost['id']);
        $arrFields = ['Name','Subid','Order', 'Postback_pass', 'Rate'];
        foreach ($arrFields as $field)
            $model->{$field} = $this->dataPost[$field];
        $arrFields = ['Postback_Affsub_id', 'Postback_Credit', 'Postback_Offer'];
        foreach ($arrFields as $field)
            $model->{$field} = $this->dataPost[$field.'_0'].'{{}}'.$this->dataPost[$field.'_1'];

        if($model->save())
            return [
                'content' => 'Đã cập nhật dữ liệu thành công'
            ];
        throw new HttpException(500, Html::errorSummary($model));
    }

    /** delete */
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        Network::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'message' => 'Đã xóa dữ liệu thành công',
        ];
    }

    public function actionGetNetwork(){
        return [
            'optionsNetwork' => (new Network())->getNetwork()
        ];
    }

    public function actionGetChienDich(){
        $dataPost = myAPI::getDataPost();
        $fields = [
            ['key' => 'Tên chiến dịch', 'label' => 'Tên chiến dịch', 'width' => '1%'],
            ['key' => 'Track link', 'label' => 'Track link', 'width' => ''],
            ['key' => 'Trạng thái', 'label' => 'Trạng thái', 'width' => '1%'],
        ];
        if (!is_null($dataPost['nhom_network'])){
            $chienDich = ChienDich::find()
                        ->andFilterWhere(['network_id' => $dataPost['nhom_network']['key']])
                        ->orderBy(['created' => SORT_DESC])
                        ->all();
        }

        return [
            'fields' => $fields,
            'chienDich' => $chienDich
        ];
    }
}
