<?php

namespace backend\controllers;

use backend\models\DanhMuc;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Cookie;
use yii\web\HttpException;

class StoreApiController extends CoreApiController
{
    /** login */
    public function actionLogin(){
        if(!isset($_POST['username']))
            throw new HttpException(500, 'Không có thông tin tên đăng nhập');
        else if(!isset($_POST['password']))
            throw new HttpException(500, 'Không có thông tin mật khẩu');
        else{
            if(empty($_POST['username']) || empty($_POST['password'])){
                throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var User $user */
                $user = User::find()
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['username']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(\Yii::$app->security->validatePassword($_POST['password'], $user->password_hash)){
                        $auth = \Yii::$app->security->generateRandomString(32);
                        $user->updateAttributes(['auth_key' => $auth]);

                        $cookies = \Yii::$app->response->cookies;
                        $cookies->add(new Cookie([
                            'name' => 'token',
                            'value' => $auth
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'username',
                            'value' => \Yii::$app->security->generateRandomString()
                        ]));
                        $cookies->add(new Cookie([
                            'name' => 'userId',
                            'value' => $user->username
                        ]));

                        return [
                            'message' => 'Đăng nhập thành công',
                            'uid' => $user->id,
                            'auth' => $auth,
                            'user' => User::findOne($user->id),
                        ];
                    }
                    else
                        throw new HttpException(500, 'Mật khẩu không đúng');
                }
            }
        }
    }

    /** login-app */
    public function actionLoginApp(){
        if(!isset($_POST['dienthoai']))
            throw new HttpException(500, 'Vui lòng điền số điện thoại để đăng nhập');
        else if(!isset($_POST['matkhau']))
            throw new HttpException(500, 'Vui lòng nhập mật khẩu');
        else{
            if(empty($_POST['dienthoai']) || empty($_POST['matkhau'])){
                throw new HttpException(500, 'Số điện thoại và mật khẩu không được để trống');
            }else{
                /** @var User $user */
                $user = User::find()
                    ->andWhere('username = :u or dien_thoai = :u', [
                        ':u' => $_POST['dienthoai']
                    ])
                    ->andFilterWhere(['status' => 10])
                    ->one();
                if(is_null($user)){
                    throw new HttpException(500, 'Tên đăng nhập hoặc số điện thoại không đúng');
                }else{
                    if(!isset($_POST['auth_key'])){
                        if(\Yii::$app->security->validatePassword($_POST['matkhau'], $user->password_hash)){
                            $auth = \Yii::$app->security->generateRandomString(32);
                            $user->updateAttributes(['auth_key' => $auth]);
                            return [
                                'message' => 'Đăng nhập thành công',
                                'user' => [
                                    'uid' => $user->id,
                                    'auth' => $auth,
                                    'ho_ten' => $user->hoten,
                                    'dien_thoai' => $user->dien_thoai,
                                    'total' => $user->total,
                                    'anhdaidien' => $user->anhdaidien,
                                ]
                            ];
                        }
                        else
                            throw new HttpException(500, 'Mật khẩu không đúng');
                    }else{
                        if($user->password_hash != $_POST['matkhau'])
                            throw new HttpException(500, 'Mật khẩu không đúng');
                        else{
                            $auth = \Yii::$app->security->generateRandomString(32);
                            $user->updateAttributes(['auth_key' => $auth]);
                            return [
                                'message' => 'Đăng nhập thành công',
                                'user' => [
                                    'uid' => $user->id,
                                    'auth' => $auth,
                                    'ho_ten' => $user->hoten,
                                    'dien_thoai' => $user->dien_thoai,
                                    'total' => $user->total,
                                    'anhdaidien' => $user->anhdaidien,
                                ]
                            ];
                        }
                    }
                }
            }
        }
    }

    /** logout */
    public function actionLogout(){
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('token');
        $cookies->remove('username');
        $cookies->remove('userId');
        return [
            'message' => 'Success',
            'status' => 200,
            'content' => 'Đã đăng xuất thành công'
        ];
    }

    /** register */
    public function actionRegister(){
        if(!isset($_POST['accept']))
            throw new HttpException(500, 'Vui lòng chấp nhận các điều khoản khi đăng ký');
        else{
            if($_POST['matkhau'] == '' || $_POST['dienthoai'] == '' || $_POST['hoten'] == '')
                throw new HttpException(500, 'Vui lòng nhập đầy đủ thông tin trên biểu mẫu');
            else if($_POST['retype_matkhau'] != $_POST['matkhau'])
                throw new HttpException(500, 'Mật khẩu nhập lại không trùng nhau');
            else if(mb_strlen($_POST['matkhau']) < 6)
                throw new HttpException(500, 'Mật khẩu dài tối thiểu 6 ký tự');
            else if(mb_strlen($_POST['dienthoai']) != 10)
                throw new HttpException(500, 'Nhập số điện thoại dài 10 ký tự');
            else{
                $model = User::findOne(['dien_thoai' => $_POST['dienthoai'], 'status' => 10]);
                if(!is_null($model))
                    throw new HttpException(500, 'Số điện thoại đã tồn tại');
                $model = User::findOne(['email' => $_POST['email'], 'status' => 10]);
                if(!is_null($model))
                    throw new HttpException(500, 'Email đã tồn tại');
                $model = new User();
                $model->username = $_POST['dienthoai'];
                $model->email = (isset($_POST['email'])?$_POST['email']:"");
                $model->dien_thoai = $_POST['dienthoai'];
                $model->hoten = $_POST['hoten'];
                $model->total = 0;
                $model->password_hash = \Yii::$app->security->generatePasswordHash($_POST['matkhau']);
                $authKey = \Yii::$app->security->generateRandomString(32);
                $model->auth_key = $authKey;
                $model->khach_hang = 1;
                if($model->save()){
                    $this->compareMaGioiThieu($_POST['ma-gioi-thieu']);
                    $ho_ten = strtoupper(substr($model->code_ho_ten, -1)).strtotime(date('d-m-Y H:i:s'));
                    $link_gioi_thieu = 'http://localhost/Vietlixi-App/app-register.html?ma_gioi_thieu='.$ho_ten;
                    $vaiTroKhachHang = VaiTro::findOne(['name' => 'Khách hàng']);
                    if(is_null($vaiTroKhachHang)){
                        $vaiTroKhachHang = new VaiTro();
                        $vaiTroKhachHang->name = 'Khách hàng';
                        $vaiTroKhachHang->save();
                    }
                    $vaiTroUser = new Vaitrouser();
                    $vaiTroUser->vaitro_id = $vaiTroKhachHang->id; // Khách hàng
                    $vaiTroUser->user_id = $model->id;
                    $vaiTroUser->save();
                    /** @var User $user */
                    $user = User::find()
                        ->select(['id', 'username', 'total', 'hoten', 'auth_key'])
                        ->andFilterWhere(['id' => $model->id])
                        ->one();

                    $user->updateAttributes(['ma_gioi_thieu'=>$ho_ten]);
                    $user->updateAttributes(['link'=>$link_gioi_thieu]);
                    return [
                        'content' => 'Chúc mừng bạn đã đăng ký tài khoản thành công',
                        'accessToken' => $authKey,
                        'refreshToken' => \Yii::$app->security->generateRandomString(32),
                        'user' => [
                            'uid' => $user->id,
                            'auth' => $user->auth_key,
                            'ho_ten' => $user->hoten,
                            'dien_thoai' => $user->dien_thoai,
                            'total' => $user->total,
                            'anhdaidien' => $user->anhdaidien,
                        ]
                    ];

                }else
                    throw new HttpException(500, strip_tags(Html::errorSummary($model)));
            }

        }
    }

    function compareMaGioiThieu($ma_gioi_thieu){
        $user = User::findOne(['ma_gioi_thieu'=>$ma_gioi_thieu]);
        $danhMuc = DanhMuc::findOne(['type' => 'Mã giới thiệu']);
        if (!is_null($user)){
            $user->updateAttributes(['vi_dien_tu'=>($user->vi_dien_tu + $danhMuc->ti_le_hoan_tien)]);
            $user->updateAttributes(['total'=>($user->total + $danhMuc->ti_le_hoan_tien)]);
        }
    }
}
