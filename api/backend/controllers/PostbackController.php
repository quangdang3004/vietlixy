<?php

namespace backend\controllers;

use backend\models\ChienDichPostback;
use backend\models\LichSuGiaoDich;
use backend\models\LichSuPostback;
use common\models\myAPI;
use yii\filters\AccessControl;

class PostbackController extends CoreApiController
{
    public function behaviors()
    {
        $arr_action = [
            'xem-chi-tiet'
        ];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('Postback', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    //xem-chi-tiet
    public function actionXemChiTiet(){
        $model = ChienDichPostback::find()
            ->select([
                'id', 'note_status',
              'trang_thai',
              'nguoi_thuc_hien',
              'image',
              'title',
              'chi_phi_chien_dich',
              'utm_source',
              'utm_medium',
              'loi_nhuan',
              'so_tien_nhan_duoc',
                'ngay_thuc_hien'
            ])
            ->andFilterWhere(['id' => $this->dataPost['postback']])
            ->one();
        $lichSuTrangThai = LichSuPostback::find()
            ->andFilterWhere(['postback_id' => $this->dataPost['postback']])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $lichSuGiaoDich = LichSuGiaoDich::find()
            ->andFilterWhere(['chien_dich_post_back_id' => $this->dataPost['postback']])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return [
            'postBack' => $model,
            'lichSuTrangThai' => $lichSuTrangThai,
            'lichSuGiaoDich' => $lichSuGiaoDich
        ];
    }
}
