<?php

namespace backend\controllers;

use backend\models\ChienDichPostback;
use backend\models\QuanLyQuaTangThanhVien;
use backend\models\QuaTangThanhVien;
use backend\models\TrangThaiQuaTangThanhVien;
use yii\helpers\Html;
use common\models\User;
use common\models\myAPI;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\filters\AccessControl;
use backend\models\LichSuGiaoDich;
use backend\models\TrangThaiGiaoDich;

class GiaoDichController extends CoreApiController
{
    public function behaviors()
    {
        $arr_action = [
            "get-data",
            "load",
            "yeu-cau-rut-tien",
            "lich-su-rut-tien",
            "lich-su-doi-qua",
            "cancel",
            "confirm-rut-tien",
            "confirm-doi-qua",
            "save-giao-dich",
            "delete",
            "xac-nhan-rut-tien",
            "xem-chi-tiet-rut-tien",
            "xem-chi-tiet-doi-qua",
            "yeu-cau-doi-qua",
            "save"
        ];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                "actions" => [$item],
                "allow" => true,
                "matchCallback" => function ($rule, $action) {
                    $action_name = strtolower(str_replace("action", "", $action->id));
                    $data = myAPI::getDataPost();
                    $uid = $data["uid"];
                    return myAPI::isAccess2("GiaoDich", $action_name, $uid);
                },
            ];
        }
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => $rules,
            ],
        ];
    }

    /** get-data */
    public function actionGetData()
    {
        $query = LichSuGiaoDich::find()
            ->andFilterWhere(["active" => 1]);

        $tongTien = 0;
        if (isset($this->dataPost["fieldsSearch"])) {
            if (count($this->dataPost["fieldsSearch"]["value"]) > 0 && !($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['type'] == ''&& $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                $arrFieldSearch = ["username", "cost"];
                foreach ($arrFieldSearch as $item) {
                    if ($this->dataPost["fieldsSearch"]["value"][$item]) {
                        $query = $query->andFilterWhere([
                            $item => $this->dataPost["fieldsSearch"]["value"][$item],
                        ]);
                    }
                }

                $arrDropDownSearch = ['trang_thai', 'type'];
                foreach ($arrDropDownSearch as $item){
                    if ((isset($this->dataPost['fieldsSearch']['value'][$item]['key'])))
                        if ($this->dataPost['fieldsSearch']['value'][$item]['key'] != '') {
                            $query = $query->andFilterWhere([$item => $this->dataPost['fieldsSearch']['value'][$item]['key']]);
                        }
                }


                if (isset($this->dataPost['fieldsSearch']['value']['tuNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['tuNgay'] != '')
                        $query = $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['tuNgay']))]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['denNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['denNgay'] != '')
                        $query = $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['denNgay']))]);
                }

                $tongTien += $query->sum('cost');
            }
        }

        if (!User::isViewAll($this->dataPost["uid"])) {
            $query->andWhere(["user_id" => $this->dataPost["uid"]]);
        }
        $totalCount = $query->count();
        $data = $query
            ->orderBy(['created'=>SORT_DESC])
            ->offset(($this->dataPost["offset"] - 1) * $this->dataPost["perPage"])
            ->limit($this->dataPost["limit"])
            ->all();

        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['type'] == ''&& $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                foreach ($data as $item) {
                    $tongTien += $item->cost;
                }
            }
        }

        return [
            "results" => $data,
            "rows" => $totalCount,
            "tongTien" => $tongTien,
            "isUpdate" => User::isViewAll($this->dataPost["uid"]),
        ];
    }

    /** lich-su-rut-tien */
    public function actionLichSuRutTien()
    {
        $query = LichSuGiaoDich::find()
            ->andWhere(["=", "trang_thai", LichSuGiaoDich::DA_DUYET])
            ->andFilterWhere(["active" => 1, 'type' => LichSuGiaoDich::RUT_TIEN]);

        $tongTien = 0;
        if (isset($this->dataPost["fieldsSearch"])) {
            if (count($this->dataPost["fieldsSearch"]["value"]) > 0 && !($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == ''&& $this->dataPost['fieldsSearch']['value']['bank_number'] == '')) {
                $arrFieldSearch = ["username", "cost", "bank_number"];
                foreach ($arrFieldSearch as $item) {
                    if ($this->dataPost["fieldsSearch"]["value"][$item]) {
                        $query = $query->andFilterWhere([
                            $item => $this->dataPost["fieldsSearch"]["value"][$item],
                        ]);
                    }
                }

//                if ((isset($this->dataPost['fieldsSearch']['value']['type']['key'])))
//                    if ($this->dataPost['fieldsSearch']['value']['type']['key'] != '') {
//                        $query = $query->andFilterWhere(['like', 'type', $this->dataPost['fieldsSearch']['value']['type']['key']]);
//                    }

//                if ((isset($this->dataPost['fieldsSearch']['value']['trang_thai']['key'])))
//                    if ($this->dataPost['fieldsSearch']['value']['trang_thai']['key'] != '') {
//                        $query = $query->andFilterWhere(['trang_thai' => $this->dataPost['fieldsSearch']['value']['trang_thai']['key']]);
//                    }

                if (isset($this->dataPost['fieldsSearch']['value']['tuNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['tuNgay'] != '')
                        $query = $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['tuNgay']))]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['denNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['denNgay'] != '')
                        $query = $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['denNgay']))]);
                }

                $tongTien += $query->sum('cost');
            }
        }
        if (!User::isViewAll($this->dataPost["uid"])) {
            $query->andWhere(["user_id" => $this->dataPost["uid"]]);
        }
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost["offsetTwo"] - 1) * $this->dataPost["perPageTwo"])
            ->orderBy(['created' => SORT_DESC])
            ->limit($this->dataPost["limitTwo"])
            ->all();


        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == ''&& $this->dataPost['fieldsSearch']['value']['bank_number'] == '')) {
                foreach ($data as $item) {
                    $tongTien += $item->cost;
                }
            }
        }
        return [
            "results" => $data,
            "tongTienTwo" => $tongTien,
            "rowTwos" => $totalCount,
        ];
    }

    /** lich-su-doi-qua */
    public function actionLichSuDoiQua()
    {
        $query = QuanLyQuaTangThanhVien::find()
            ->andWhere(["=", "trang_thai", LichSuGiaoDich::DA_DUYET])
            ->andFilterWhere(["active" => 1]);

        $tongTien = 0;
        if (isset($this->dataPost["fieldsSearch"])) {
            if (count($this->dataPost["fieldsSearch"]["value"]) > 0 && !($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['hoten'] == ''
                    && $this->dataPost['fieldsSearch']['value']['name'] == '' && $this->dataPost['fieldsSearch']['value']['tong_tien'] == '' && $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                $arrFieldSearch = ["name", "hoten", "tong_tien"];
                foreach ($arrFieldSearch as $item) {
                    if ($this->dataPost["fieldsSearch"]["value"][$item]) {
                        $query = $query->andFilterWhere([
                            $item => $this->dataPost["fieldsSearch"]["value"][$item],
                        ]);
                    }
                }

                if ((isset($this->dataPost['fieldsSearch']['value']['type']['key'])))
                    if ($this->dataPost['fieldsSearch']['value']['type']['key'] != '') {
                        $query = $query->andFilterWhere(['like', 'type', $this->dataPost['fieldsSearch']['value']['type']['key']]);
                    }

                if ((isset($this->dataPost['fieldsSearch']['value']['trang_thai']['key'])))
                    if ($this->dataPost['fieldsSearch']['value']['trang_thai']['key'] != '') {
                        $query = $query->andFilterWhere(['trang_thai' => $this->dataPost['fieldsSearch']['value']['trang_thai']['key']]);
                    }

                if (isset($this->dataPost['fieldsSearch']['value']['tuNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['tuNgay'] != '')
                        $query = $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['tuNgay']))]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['denNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['denNgay'] != '')
                        $query = $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['denNgay']))]);
                }

                $tongTien += $query->sum('tong_tien');
            }
        }
        if (!User::isViewAll($this->dataPost["uid"])) {
            $query->andWhere(["user_id" => $this->dataPost["uid"]]);
        }
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost["offsetOne"] - 1) * $this->dataPost["perPageOne"])
            ->orderBy(['created' => SORT_DESC])
            ->limit($this->dataPost["limitOne"])
            ->all();


        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['hoten'] == ''
                    && $this->dataPost['fieldsSearch']['value']['name'] == '' && $this->dataPost['fieldsSearch']['value']['tong_tien'] == '' && $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                foreach ($data as $item) {
                    $tongTien += $item->tong_tien;
                }
            }
        }
        return [
            "results" => $data,
            "tongTien" => $tongTien,
            "rowOnes" => $totalCount,
        ];
    }

    /** yeu-cau-doi-qua */
    public function actionYeuCauDoiQua()
    {
        $query = QuanLyQuaTangThanhVien::find()
            ->andWhere(["trang_thai" => QuanLyQuaTangThanhVien::CHO_DUYET])
            ->andFilterWhere(["active" => 1]);

        $tongTien = 0;
        if (isset($this->dataPost["fieldsSearch"])) {
            if (count($this->dataPost["fieldsSearch"]["value"]) > 0 && !($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['name'] == '' && $this->dataPost['fieldsSearch']['value']['tong_tien'] == '' && $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                $arrFieldSearch = ["name", "username", "tong_tien"];
                foreach ($arrFieldSearch as $item) {
                    if ($this->dataPost["fieldsSearch"]["value"][$item]) {
                        $query = $query->andFilterWhere([
                            $item => $this->dataPost["fieldsSearch"]["value"][$item],
                        ]);
                    }
                }

                if ((isset($this->dataPost['fieldsSearch']['value']['type']['key'])))
                    if ($this->dataPost['fieldsSearch']['value']['type']['key'] != '') {
                        $query = $query->andFilterWhere(['like', 'type', $this->dataPost['fieldsSearch']['value']['type']['key']]);
                    }

                if ((isset($this->dataPost['fieldsSearch']['value']['trang_thai']['key'])))
                    if ($this->dataPost['fieldsSearch']['value']['trang_thai']['key'] != '') {
                        $query = $query->andFilterWhere(['trang_thai' => $this->dataPost['fieldsSearch']['value']['trang_thai']['key']]);
                    }

                if (isset($this->dataPost['fieldsSearch']['value']['tuNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['tuNgay'] != '')
                        $query = $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['tuNgay']))]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['denNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['denNgay'] != '')
                        $query = $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['denNgay']))]);
                }

                $tongTien += $query->sum('tong_tien');
            }
        }
        if (!User::isViewAll($this->dataPost["uid"])) {
            $query->andWhere(["user_id" => $this->dataPost["uid"]]);
        }
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost["offsetOne"] - 1) * $this->dataPost["perPageOne"])
            ->orderBy(['created' => SORT_DESC])
            ->limit($this->dataPost["limitOne"])
            ->all();
        $user = User::findOne($this->dataPost["uid"]);

        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['name'] == '' && $this->dataPost['fieldsSearch']['value']['tong_tien'] == '' && $this->dataPost['fieldsSearch']['value']['trang_thai'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == '')) {
                foreach ($data as $item) {
                    $tongTien += $item->tong_tien;
                }
            }
        }
        return [
            "results" => $data,
            "rowOnes" => $totalCount,
            "tongTien" => $tongTien,
            "total" => $user->total,
            "isUpdate" => User::isViewAll($this->dataPost["uid"]),
        ];
    }
    /** yeu-cau-rut-tien */
    public function actionYeuCauRutTien()
    {
        $query = LichSuGiaoDich::find()
            ->andWhere(["trang_thai" => LichSuGiaoDich::CHO_DUYET, "type" => 'Rút tiền'])
            ->andFilterWhere(["active" => 1]);

        $tongTien = 0;
        if (isset($this->dataPost["fieldsSearch"])) {
            if (count($this->dataPost["fieldsSearch"]["value"]) > 0 && !($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == ''&& $this->dataPost['fieldsSearch']['value']['bank_number'] == '')) {
                $arrFieldSearch = ["username", "cost", "bank_number"];
                foreach ($arrFieldSearch as $item) {
                    if ($this->dataPost["fieldsSearch"]["value"][$item]) {
                        $query = $query->andFilterWhere([
                            $item => $this->dataPost["fieldsSearch"]["value"][$item],
                        ]);
                    }
                }

//                if ((isset($this->dataPost['fieldsSearch']['value']['type']['key'])))
//                    if ($this->dataPost['fieldsSearch']['value']['type']['key'] != '') {
//                        $query = $query->andFilterWhere(['like', 'type', $this->dataPost['fieldsSearch']['value']['type']['key']]);
//                    }

//                if ((isset($this->dataPost['fieldsSearch']['value']['trang_thai']['key'])))
//                    if ($this->dataPost['fieldsSearch']['value']['trang_thai']['key'] != '') {
//                        $query = $query->andFilterWhere(['trang_thai' => $this->dataPost['fieldsSearch']['value']['trang_thai']['key']]);
//                    }

                if (isset($this->dataPost['fieldsSearch']['value']['tuNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['tuNgay'] != '')
                        $query = $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['tuNgay']))]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['denNgay'])) {
                    if ($this->dataPost['fieldsSearch']['value']['denNgay'] != '')
                        $query = $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['denNgay']))]);
                }

                $tongTien += $query->sum('cost');
            }
        }
        if (!User::isViewAll($this->dataPost["uid"])) {
            $query->andWhere(["user_id" => $this->dataPost["uid"]]);
        }
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost["offsetTwo"] - 1) * $this->dataPost["perPageTwo"])
            ->orderBy(['created' => SORT_DESC])
            ->limit($this->dataPost["limitTwo"])
            ->all();
        $user = User::findOne($this->dataPost["uid"]);

        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['denNgay'] == '' && $this->dataPost['fieldsSearch']['value']['username'] == ''
                    && $this->dataPost['fieldsSearch']['value']['cost'] == '' && $this->dataPost['fieldsSearch']['value']['tuNgay'] == ''&& $this->dataPost['fieldsSearch']['value']['bank_number'] == '')) {
                foreach ($data as $item) {
                    $tongTien += $item->cost;
                }
            }
        }
        return [
            "results" => $data,
            "rowTwos" => $totalCount,
            "tongTienTwo" => $tongTien,
            "total" => $user->total,
            "isUpdate" => User::isViewAll($this->dataPost["uid"]),
        ];
    }

    /** load */
    public function actionLoad()
    {
        $user = User::findOne($this->dataPost["uid"]);
        if (isset($this->dataPost["id"])) {
            $lich_su_giao_dich = LichSuGiaoDich::findOne($this->dataPost["id"]);
            if (is_null($lich_su_giao_dich)) {
                throw new HttpException(500, "Giao dịch không tồn tại");
            }
        }

        return [
            "soTienConLai" => $user->total,
            "giaoDich" => isset($lich_su_giao_dich) ? $lich_su_giao_dich : null,
        ];
    }

    /** save */
    public function actionSave()
    {
        if ($this->dataPost["id"]) {
            $giao_dich = LichSuGiaoDich::findOne($this->dataPost["id"]);
        } else {
            $giao_dich = new LichSuGiaoDich();
            $giao_dich->user_id = $this->dataPost["uid"];
        }
        $giao_dich->cost = doubleval(str_replace(",", "", $this->dataPost["cost"]));
        $giao_dich->type = LichSuGiaoDich::RUT_TIEN;

        $user = User::findOne($this->dataPost["uid"]);
        $so_tien_ung_rut = LichSuGiaoDich::find()->andWhere([
            "type" => LichSuGiaoDich::RUT_TIEN,
            "trang_thai" => LichSuGiaoDich::CHO_DUYET,
        ]);
        if ($this->dataPost["id"]) {
            $so_tien_ung_rut->andWhere(["<>", "id", $giao_dich]);
        }
        $so_tien_ung_rut = $so_tien_ung_rut->sum("cost");
        if ($user->total < $giao_dich->cost + $so_tien_ung_rut) {
            throw new HttpException(500, "Số tiền rút vượt quá số tiền trong ví");
        }

        if (!$giao_dich->save()) {
            throw new HttpException(500, Html::errorSummary($giao_dich));
        }

        return [
            "title" => "Lưu giao dịch",
            "content" => "Đã lưu giao dịch thành công",
        ];
    }

    /** confirm */
    public function actionConfirmRutTien()
    {
        $giao_dich = LichSuGiaoDich::findOne(['id' => $this->dataPost["giaoDichID"]]);
        if ($giao_dich->trang_thai != LichSuGiaoDich::DA_DUYET) {
            $giao_dich->updateAttributes([
                "trang_thai" => LichSuGiaoDich::DA_DUYET,
                "nguoi_duyet_id" => $this->dataPost["uid"],
            ]);

            $user = User::findOne(['id' => $giao_dich->user_id]);
            $user->updateAttributes([
                "total" => $user->total - $giao_dich->cost,
                "current_total" => $giao_dich->cost,
                "pending_total" => $user->pending_total - $giao_dich->cost,
                "vi_dien_tu" => $user->vi_dien_tu - $giao_dich->cost,
            ]);

            $trang_thai_giao_dich = new TrangThaiGiaoDich();
            $trang_thai_giao_dich->giao_dich_id = $giao_dich->id;
            $trang_thai_giao_dich->trang_thai = LichSuGiaoDich::DA_DUYET;
            $trang_thai_giao_dich->user_id = $this->dataPost["uid"];
            if (!$trang_thai_giao_dich->save()) {
                throw new HttpException(500, Html::errorSummary($trang_thai_giao_dich));
            }
        }

        return [
            "title" => "Lịch sử giao dịch",
            "content" => "Đã cập nhật giao dịch thành công",
        ];
    }
    /** confirm */
    public function actionConfirmDoiQua()
    {
        $giao_dich = QuaTangThanhVien::findOne(['id' => $this->dataPost["quaTangID"]]);
        if ($giao_dich->trang_thai != QuaTangThanhVien::DA_DUYET) {
            $giao_dich->updateAttributes([
                "trang_thai" => QuaTangThanhVien::DA_DUYET,
                "nguoi_duyet_id" => $this->dataPost["uid"],
                "ma_the" => $this->dataPost["ma_the"],
                "so_seri" => $this->dataPost['so_seri']
            ]);

            $user = User::findOne(['id' => $giao_dich->user_id]);
            $user->updateAttributes([
                "total" => $user->total - $giao_dich->tong_tien,
                "current_total" =>$giao_dich->tong_tien,
                "vi_dien_tu" => $user->vi_dien_tu - $giao_dich->tong_tien,
            ]);

            $trang_thai_giao_dich = new TrangThaiQuaTangThanhVien();
            $trang_thai_giao_dich->qua_tang_thanh_vien_id = $giao_dich->id;
            $trang_thai_giao_dich->trang_thai = LichSuGiaoDich::DA_DUYET;
            $trang_thai_giao_dich->user_id = $this->dataPost["uid"];
            if (!$trang_thai_giao_dich->save()) {
                throw new HttpException(500, Html::errorSummary($trang_thai_giao_dich));
            }
        }

        return [
            "title" => "Lịch sử giao dịch",
            "content" => "Đã cập nhật giao dịch thành công",
        ];
    }

    /** cancel */
    public function actionCancel()
    {
        $giao_dich = LichSuGiaoDich::findOne($this->dataPost["id"]);
        if ($giao_dich->trang_thai != LichSuGiaoDich::DA_HUY) {
            $giao_dich->updateAttributes([
                "trang_thai" => LichSuGiaoDich::DA_HUY,
                "nguoi_duyet_id" => $this->dataPost["uid"],
            ]);
            $giao_dich->user->updateAttributes([
                "total" => $giao_dich->user->total - $giao_dich->cost,
            ]);

            $trang_thai_giao_dich = new TrangThaiGiaoDich();
            $trang_thai_giao_dich->giao_dich_id = $giao_dich->id;
            $trang_thai_giao_dich->trang_thai = LichSuGiaoDich::DA_HUY;
            $trang_thai_giao_dich->user_id = $this->dataPost["uid"];
            if (!$trang_thai_giao_dich->save()) {
                throw new HttpException(500, Html::errorSummary($trang_thai_giao_dich));
            }
        }

        return [
            "title" => "Lịch sử giao dịch",
            "content" => "Đã cập nhật giao dịch thành công",
        ];
    }

    /** delte */
    public function actionDelete()
    {
        $giao_dich = LichSuGiaoDich::findOne(["id" => $this->dataPost["id"]]);
        $giao_dich->updateAttributes(["active" => 0]);

        return [
            "title" => "Xóa giao dịch",
            "content" => "Đã xóa giao dịch thành công",
        ];
    }

    /** Xac nhan rut tien*/
    public function actionXacNhanRutTien()
    {
        $query = LichSuGiaoDich::find()
            ->andFilterWhere(['active' => 1, 'id' => $this->dataPost['giaoDichID']])
            ->one();

        $query->updateAttributes(['trang_thai' => $query->trang_thai == LichSuGiaoDich::CHO_DUYET ? LichSuGiaoDich::DA_DUYET : LichSuGiaoDich::CHO_DUYET]);
        return [
            "results" => $query
        ];
    }

    public function actionXemChiTietRutTien()
    {
        $model = LichSuGiaoDich::find()
            ->select([
                'id', 'user_id', 'chien_dich_post_back_id',
                'cost',
                'type',
                'created',
                'updated',
                'trang_thai',
                'ghi_chu',
                'username',
                'bank_name',
                'bank_number'
            ])
            ->andFilterWhere(['id' => $this->dataPost['giaoDich']])
            ->one();

        $ho_ten = User::findOne(['username' => $model->username]);
        $chien_dich = ChienDichPostback::findOne(['id' => $model->chien_dich_post_back_id]);

        return [
            'giaoDich' => $model,
            'hoTen' => $ho_ten->hoten,
            'zalo'=>"https://zalo.me/".$ho_ten->dien_thoai,
            'chienDich' => isset($chien_dich->title) ? $chien_dich->title : '',
        ];
    }

    public function actionXemChiTietDoiQua(){
        $model = QuanLyQuaTangThanhVien::find()
            ->select(['id' ,'qua_tang_id', 'khach_hang_id', 'trang_thai', 'created',
                'user_id', 'so_luong', 'don_gia', 'tong_tien', 'name', 'hoten', 'username'])
            ->andFilterWhere(['id' => $this->dataPost['giaoDich']])
            ->one();
        $ho_ten = User::findOne(['username' => $model->username]);

        return [
          'giaoDich' => $model,
            'zalo'=>"https://zalo.com/".$ho_ten->dien_thoai,

        ];
    }
}
