<?php

namespace backend\controllers;

use common\models\myAPI;
use backend\models\VaiTro;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\helpers\Html;

class VaiTroController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-all-vai-tro', 'get-data', 'save', 'load', 'delete'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('VaiTro', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    //get-all-vai-tro
    public function actionGetAllVaiTro(){
        return ['optionVaiTro' => VaiTro::getAllVaiTro()];
    }

    // get-data
    public function actionGetData(){
        $query = VaiTro::find();
        $totalCount = $query->count();
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] == '')
            $model = new VaiTro();
        else
            $model = VaiTro::findOne($this->dataPost['id']);
        $model->name = $this->dataPost['name'];
        if($model->save())
            return [
                'content' => 'Đã lưu thông tin vai trò '.$model->name
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }

    /** load */
    public function actionLoad(){
        $model = VaiTro::findOne($this->dataPost['vai_tro']);
        return [
            'result' => $model
        ];
    }

    /** delete */
    public function actionDelete(){
        $model = VaiTro::findOne($this->dataPost['vai_tro']);
        if($model->delete())
            return [
                'message' => 'Đã xóa dữ liệu vai trò '.$model->name.' thành công',
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }
}
