<?php

namespace backend\controllers;

use backend\models\ChienDichPostback;
use backend\models\DanhMuc;
use backend\models\QuanLyKhachHang;
use backend\models\QuanLyUser;
use backend\models\TrangThaiAffiliate;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class UserController extends CoreApiController
{
    public function behaviors()
    {
        $arr_action = ['get-affiliate', 'get-data', 'save', 'load', 'delete', 'get-tree', 'load-thong-tin-thanh-toan',
            'get-affs', 'chuyen-trang-thai-nhieu-aff', 'load-profile', 'update-profile', 'load-affiliate', 'xuat-file-excel'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('User', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    // get-affiliate
    public function actionGetAffiliate(){
        $selectArr = [
            'id', 'username', 'email', 'hoten', 'dien_thoai', 'dia_chi', 'cmnd', 'CIF', 'household_address',
        ];

        $data = QuanLyKhachHang::find()
            ->select($selectArr)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $arr = [];
        /** @var QuanLyKhachHang $item */
        foreach ($data as $item){
            $arr[] = ['key' => $item->id, 'label' =>  "{$item->hoten}"];
        }

        return [
            'results' => $arr
        ];
    }

    // get-data
    public function actionGetData()
    {
        $dataPost = myAPI::getDataPost();
        $selectArr = [
            'id', 'username', 'email', 'hoten', 'dien_thoai', 'total',
            'pending_total', 'current_total', 'trang_thai', 'password_hash',
            'auth_key', 'vi_dien_tu'
        ];
        if($dataPost['type'] == 'Khách hàng'){
            $query = QuanLyKhachHang::find();
        }else if($dataPost['type'] == 'Thành viên'){
            $query = QuanLyUser::find();
            $selectArr [] = 'vai_tro';
        }else{
            $selectArr [] = 'vai_tro';
            $query = QuanLyUser::find();
        }

        $data = $query->andFilterWhere(['status' => 10])
            ->select($selectArr)
            ->orderBy(['id' => SORT_DESC])
            ->offset(($dataPost['offset'] - 1) * $dataPost['perPage'])
            ->limit($dataPost['limit']);
        if(isset($this->dataPost['fieldsSearch'])){
            if(count($this->dataPost['fieldsSearch']['value']) > 0){
                $arrFieldSearch = ['username','hoten', 'dien_thoai'];
                foreach ($arrFieldSearch as $item) {
                    if(trim($item)!='')
                        $data = $data->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                }
                if (isset($this->dataPost['fieldsSearch']['value']['vai_tro']['key'])){
                    if ($this->dataPost['fieldsSearch']['value']['vai_tro']['key'] != ''){
                        $data = $data->andFilterWhere(['like', 'vai_tro', $this->dataPost['fieldsSearch']['value']['vai_tro']['key']]);
                    }
                }
            }
        }
        $totalCount = $data->count();

        $data = $data->all();
        return [
            'users' => $data,
            'rows' => $totalCount
        ];
    }

    //save
    public function actionSave(){
        if(!isset($this->dataPost['id'])){
            $model = User::findOne(['dien_thoai' => $this->dataPost['dien_thoai']]);
            if(!is_null($model))
                throw new HttpException(500, 'Số điện thoại đã tồn tại');
            else
                $model = new User();
        }else {
            if($this->dataPost['id'] == '')
                $model = new User();
            else{
                $model = User::findOne($this->dataPost['id']);
                $model->updateAttributes(['status' => 10]);
            }
        }

        foreach ($this->dataPost as $attr => $value) {
            if(!in_array($attr, [ 'auth', 'uid', 'dai_ly_id', 'mat_khau', 'vai_tro']))
                $model->{$attr} = $this->dataPost[$attr];
        }

        if (isset($this->dataPost['vai_tro'])) {
            $model->vai_tro = $this->dataPost['vai_tro'];
        }

        $model->khach_hang = isset($this->dataPost['khach_hang']) ? $this->dataPost['khach_hang'] : 0;
        $model->thanh_vien = isset($this->dataPost['thanh_vien']) ? $this->dataPost['thanh_vien'] : 0;
        $title = ' affiliate';
        if($model->khach_hang == 0){
            $title = ' khách hàng ';
            if(!$model->isNewRecord){
                $oldUser = User::findOne($model->id);
                if($this->dataPost['password_hash'] != $oldUser->password_hash)
                    $model->password = $this->dataPost['password_hash'];
            }
            else
                $model->password = $this->dataPost['password_hash'];

            $model->username = $this->dataPost['username'];
        }
        $isNewRecord = $model->isNewRecord;
        if($model->save()){
            if($model->khach_hang == 1 && $isNewRecord){
                $vaiTroKH = VaiTro::findOne(['name' => 'Affiliate']);
                if(!is_null($vaiTroKH)){
                    $vaiTroUser = new Vaitrouser();
                    $vaiTroUser->user_id = $model->id;
                    $vaiTroUser->vaitro_id = $vaiTroKH->id;
                    $vaiTroUser->save();
                }
            }
            return [
                'result' => 'success',
                'content' => 'Đã lưu thông tin '.$title.' '.$model->hoten
            ];
        }else
            throw new HttpException(500, 'Tên người dùng đã tồn tại');
    }

    //load-affiliate
    public function actionLoadAffiliate(){
        if (isset($this->dataPost['khachHang'])){
            if ($this->dataPost['khachHang'] != ''){
                $user = QuanLyKhachHang::findOne(['id' => $this->dataPost['khachHang']]);
            }

            if (is_null($user)){
                throw new HttpException(500, "Người dùng không tồn tại");
            }

            $chienDich = ChienDichPostback::find()->andFilterWhere(['nguoi_thuc_hien' => $user->username])->all();


            return [
                'user' => $user,
                'chienDich' => $chienDich
            ];
        }

        throw new HttpException(500, 'Không xác thực được người dùng');
    }

    // load
    public function actionLoad(){
        /** @var User $user */
        $user = User::find()->andFilterWhere(['id' => $this->dataPost['khach_hang']])
            ->select(['id', 'hoten', 'dien_thoai', 'password_hash', 'email', 'ngay_sinh', 'username', 'dia_chi'])
            ->one();
        if($user->khu_vuc_id != ''){
            $khuVuc = DanhMuc::findOne($user->khu_vuc_id);
            $user->khu_vuc_id = [
                'label' => $khuVuc->name,
                'value' => $khuVuc->id
            ];
        }
        if($user->dai_ly_id != ''){
            $daiLy = User::findOne($user->dai_ly_id);
            $user->dai_ly_id = [
                'label' => $daiLy->hoten,
                'value' => $daiLy->id
            ];
        }

        if(isset($this->dataPost['thanh_vien'])){
            $vaiTroUser = [];
            foreach ($user->vaitrousers as $vaitrouser) {
                $vaiTroUser[] = intval($vaitrouser->vaitro_id);
            }
            $vaiTro = VaiTro::getAllVaiTro();
            $user->vai_tro = ArrayHelper::map(Vaitrouser::findAll(['user_id' => $this->dataPost['khach_hang']]), 'vaitro_id', 'vaitro_id');

            if(!is_null($user))
                return [
                    'user' => $user,
                    'vai_tro_user' => $vaiTroUser,
                    'optionsVaiTro' => $vaiTro
                ];
        }else{
            if(!is_null($user))
                return $user;
            throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
        }
    }

    // delete
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        if ($dataPost['khach_hang'] != 1) {
            User::updateAll(['status' => 0], ['id' => $dataPost['khach_hang']]);
        }
        return [
            'message' => 'Đã xóa thông tin thành công',
        ];
    }

    // load-thong-tin-thanh-toan
    public function actionLoadThongTinThanhToan(){
        $model = User::find()
            ->andFilterWhere(['id' => $this->dataPost['khach_hang']])
            ->select(['hoten', 'dien_thoai', 'so_tien_can_thanh_toan', 'so_tien_da_thanh_toan', 'con_no', 'id', 'so_tien', 'khuyen_mai'])
            ->one();
        if(is_null($model))
            throw new HttpException(500, 'Không có thông tin khách hàng tương ứng');
        else
            return $model;
    }

    /*get-affs*/
    public function actionGetAffs(){
        $trangThai = [
            ['key' => TrangThaiAffiliate::PENDING, 'label' => TrangThaiAffiliate::PENDING],
            ['key' => TrangThaiAffiliate::PAUSE, 'label' => TrangThaiAffiliate::PAUSE],
            ['key' => TrangThaiAffiliate::APPROVED, 'label' => TrangThaiAffiliate::APPROVED],
            ['key' => TrangThaiAffiliate::BANNED, 'label' => TrangThaiAffiliate::BANNED],
        ];

        $donHang = [];
        foreach ($this->dataPost['donHangDaChon'] as $item){
            $donHang[] = User::find()
                ->select(['id',  'username', 'trang_thai', 'hoten'])
                ->andFilterWhere(['id' => $item['id']])
                ->andFilterWhere(['status' => 10])
                ->one();

        }
        return [
            'donHang' => $donHang,
            'trangThai' => $trangThai
        ];
    }

    //load-profile
    public function actionLoadProfile(){
        if (isset($this->dataPost['uid'])){
            $user = User::findOne(['id' => $this->dataPost['uid']]);

            if (is_null($user))
                throw new HttpException(500, 'Không tìm thấy thông tin người dùng đang đăng nhập');

            return [
                'user' => $user,
            ];
        }

    }

    //update-profile
    public function actionUpdateProfile()
    {
        if ($this->dataPost['id'] != $this->dataPost['uid'])
            throw new HttpException(500, 'Không thể cập nhật thông tin do thông tin người dùng đang đăng nhập không chính xác');
        else {
            $user = User::findOne($this->dataPost['id']);
            if ($user->email != trim($this->dataPost['email'])) {
                $existUser = User::find()->andFilterWhere(['email' => trim($this->dataPost['email'])])
                    ->andWhere('id <> :i', [':i' => $user->id])
                    ->one();
                if (!is_null($existUser))
                    throw new HttpException(500, 'Email đã tồn tại');
            }

            if ($user->dien_thoai != trim($this->dataPost['dien_thoai'])) {
                $existUser = User::find()->andFilterWhere(['dien_thoai' => trim($this->dataPost['dien_thoai'])])
                    ->andWhere('id <> :i', [':i' => $user->id])
                    ->one();
                if (!is_null($existUser))
                    throw new HttpException(500, 'Số điện thoại đã tồn tại');
            }

            $dataAttr = [
                'email' => $this->dataPost['email'],
                'hoten' => $this->dataPost['hoten'],
                'dien_thoai' => $this->dataPost['dien_thoai'],
                'ngay_sinh' => date("Y-m-d",strtotime($this->dataPost['ngay_sinh'])),
            ];
            if ($user->password_hash != $this->dataPost['password_hash'])
                $dataAttr['password_hash'] = Yii::$app->security->generatePasswordHash($this->dataPost['password_hash']);
            else
                $dataAttr['password_hash'] = $this->dataPost['password_hash'];
            $user->updateAttributes($dataAttr);

            return [
                'content' => 'Đã cập nhật thông tin cá nhân thành công',
                'data'=>$user,
            ];

        }
    }

    //xuat-file-excel
    public function actionXuatFileExcel()
    {

    }
}
