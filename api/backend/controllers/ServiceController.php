<?php

namespace backend\controllers;

use backend\models\Banner;
use backend\models\ChienDich;
use backend\models\ChienDichPostback;
use backend\models\ChiTietBanner;
use backend\models\DanhMuc;

//use backend\models\LichSuDongBoChienDich;
use backend\models\LichSuGiaoDich;
use backend\models\LichSuPostback;
use backend\models\Postback;
use backend\models\QuanLyBanner;
use backend\models\QuanLyPostback;
use backend\models\QuanLyQuaTangThanhVien;
use backend\models\QuaTang;
use backend\models\QuaTangThanhVien;
use backend\models\ThucHienChienDich;
use backend\models\TiLeChiaSeHoaHong;
use backend\models\TrangThaiChienDich;
use backend\models\TrangThaiGiaoDich;
use backend\models\TrangThaiQuaTangThanhVien;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\UploadedFile;

class ServiceController extends CoreApiAppController
{
    //init-index
    public function actionInitIndex()
    {
        $kiemTien = ChienDich::find()
            ->select(['id', 'image', 'title', 'Capped', 'chi_phi_chien_dich', 'type_hoa_hong'])
            ->andFilterWhere([
                    'danh_muc_id' => 73,
                    'noi_bat' => 1,
                    'active' => 1,
                    'trang_thai' => ChienDich::HOAT_DONG]
            )
            ->orderBy(['vi_tri_hien_thi' => SORT_ASC])
            ->all();

        $hoanTienShopback = DanhMuc::find()
            ->select(['id', 'image', 'ti_le_hoan_tien', 'name'])
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['noi_bat' => 1])
            ->andFilterWhere(['type' => DanhMuc::THUONG_HIEU])
            ->orderBy(['vi_tri' => SORT_ASC])
            ->all();

        $dichVu = DanhMuc::find()
            ->select(['id', 'image', 'name'])
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['noi_bat' => 1])
            ->andFilterWhere(['type' => DanhMuc::DICH_VU])
            ->orderBy(['vi_tri' => SORT_ASC])
            ->all();

        $user = null;
        if (isset($_POST['user'])) {
            if ($_POST['user'] != '' && !is_null($_POST['user'])) {
                $user = User::findOne($_POST['user']['uid']);
                $user->updateAttributes(['so_lan_truy_cap' => $user->so_lan_truy_cap + 1]);
            }

        }
        return [
            'kiemTien' => $kiemTien,
            'shopbackHoanTien' => $hoanTienShopback,
            'dichVu' => $dichVu,
            'user' => is_null($user) ? null : [
                'uid' => $user->id,
                'auth' => $user->auth_key,
                'ho_ten' => $user->hoten,
                'dien_thoai' => $user->dien_thoai,
                'total' => $user->total,
                'anhdaidien' => $user->anhdaidien,
                'so_lan_truy_cap' => $user->so_lan_truy_cap,
            ],
            'topBanner' => (new ChiTietBanner())->getBannersByType(Banner::DAU_TRANG),
            'middleBanner' => (new ChiTietBanner())->getBannersByType(Banner::GIUA_TRANG),
            'bottomBanner' => (new ChiTietBanner())->getBannersByType(Banner::CUOI_TRANG),
        ];
    }

    //detail
    public function actionDetail()
    {
        if (!isset($_POST['type']) && !isset($_POST['value'])) {
            throw new HttpException(500, 'Không có thông tin mã dữ liệu cần xem chi tiết');
        } else {
            $title = $_POST['type'];
            $models = [];

            if ($_POST['type'] == 'Kiếm tiền') {
                $chienDich = ChienDich::find()
                    ->select(['id', 'title', 'conversion_requirements', 'image',
                        'Capped', 'type_hoa_hong', 'chi_phi_chien_dich', 'preview_offer'])
                    ->andFilterWhere(
                        [
                            'id' => $_POST['value'],
                            'active' => 1,
                        ]
                    )->one();
                if (!is_null($chienDich)) {
                    $models[] = $chienDich;
                    $router = 'app-blog-post.html';
                } else
                    $router = '';
            } elseif ($_POST['type'] == 'Vay tiền mùa dịch') {
                $chienDich = ChienDich::find()
                    ->select(['id', 'title', 'conversion_requirements', 'image',
                        'Capped', 'type_hoa_hong', 'chi_phi_chien_dich', 'preview_offer', 'ti_le_hoan_tien', 'type_hoan_tien'])
                    ->andFilterWhere(
                        [
                            'id' => $_POST['value'],
                            'active' => 1,
                        ]
                    )->one();
                if (!is_null($chienDich)) {
                    $models[] = $chienDich;
                    $router = 'app-blog-post.html';
                } else
                    $router = '';
            } else {
                $models = ChienDich::find()->andFilterWhere([
                    'danh_muc_id' => $_POST['value'],
                    'active' => 1,
                ])
                    ->select(['id', 'title', 'image', 'track_link', 'ti_le_hoan_tien', 'type_hoan_tien'])
                    ->orderBy([
                        'vi_tri_hien_thi' => SORT_ASC,
                    ])->all();
                $danh_muc = DanhMuc::findOne($_POST['value']);
                if (!is_null($danh_muc)) {
                    $title = $danh_muc->name;
                }
                if (count($models) > 0)
                    $router = 'app-transactions.html';
                else
                    $router = '';
            }
            return [
                'models' => $models,
                'router' => $router,
                'title' => $title,
            ];
        }
    }

    //get-other-models
    public function actionGetOtherModels()
    {
        if (!isset($_POST['model'])) {
            throw new HttpException(500, 'Không có thông tin chiến dịch');
        } else {
            $chienDich = ChienDich::findOne($_POST['model']);
            if (is_null($chienDich)) {
                throw new HttpException(500, 'Không có thông tin chiến dịch');
            } else if ($chienDich->active == 0 || $chienDich->trang_thai == ChienDich::KHONG_HOAT_DONG) {
                throw new HttpException(500, 'Chiến dịch không hoạt động');
            } else {
                $models = ChienDich::find()
                    ->andFilterWhere([
                        'active' => 1,
                        'trang_thai' => ChienDich::HOAT_DONG,
                    ])
                    ->andWhere('(danh_muc_id = :d) and id <> :i', [
                        ':d' => $chienDich->danh_muc_id,
                        ':i' => $chienDich->id,
                    ])
                    ->orderBy(['RAND ()' => ''])
                    ->select(['id', 'image', 'title', 'Capped', 'chi_phi_chien_dich', 'type_hoa_hong'])
                    ->limit(6)
                    ->all();
                return [
                    'models' => $models,
                ];
            }
        }
    }

    //get-other-models-vay-tien
    public function actionGetOtherModelsVayTien()
    {
        if (!isset($_POST['model'])) {
            throw new HttpException(500, 'Không có thông tin chiến dịch');
        } else {
            $chienDich = ChienDich::findOne($_POST['model']);
            if (is_null($chienDich)) {
                throw new HttpException(500, 'Không có thông tin chiến dịch');
            } else if ($chienDich->active == 0 || $chienDich->trang_thai == ChienDich::KHONG_HOAT_DONG) {
                throw new HttpException(500, 'Chiến dịch không hoạt động');
            } else {
                $models = ChienDich::find()
                    ->andFilterWhere([
                        'active' => 1,
                        'trang_thai' => ChienDich::HOAT_DONG,
                    ])
                    ->andWhere('(danh_muc_id = :d) and id <> :i', [
                        ':d' => $chienDich->danh_muc_id == 73 ? 92 : 73,
                        ':i' => $chienDich->id,
                    ])
                    ->orderBy(['RAND ()' => ''])
                    ->select(['id', 'image', 'title', 'Capped', 'chi_phi_chien_dich', 'type_hoa_hong'])
                    ->limit(6)
                    ->all();
                return [
                    'models' => $models,
                ];
            }
        }
    }

    //dang-ky-chien-dich
    public function actionDangKyChienDich()
    {
        $time = 10;
        $contentFailed = 'Vui lòng đăng nhập!<br/>Bạn sẽ được chuyển tới trang <a href="https://vietlixi.com/app-login.html" title="Đăng nhập hệ thống">đăng nhập</a> sau <span id="thoi-gian">' . $time . '</span> giây hoặc hãy nhấn vào <a href="https://vietlixi.com/app-login.html" title="Đăng nhập hệ thống">đây</a> để chuyển trang.';
        if (!isset($_POST['model'])) {
            throw new HttpException(500, 'Không có thông tin chiến dịch');
        } else if ($_POST['model'] == '' || is_null($_POST['model']) || empty($_POST['model'])) {
            throw new HttpException(500, 'Không có thông tin chiến dịch');
        } else {
            if (!isset($_POST['user'])) {
                return [
                    'success' => 'failed',
                    'type' => 'logged',
                    'content' => $contentFailed,
                    'model' => $_POST['model'],
                    'time' => $time,
                ];
            } else if ($_POST['user'] == '' || is_null($_POST['user']) || empty($_POST['user'])) {
                return [
                    'success' => 'failed',
                    'type' => 'logged',
                    'content' => $contentFailed,
                    'model' => $_POST['model'],
                    'time' => $time,
                ];
            } else {
                /** @var ChienDich $chienDich */
                $chienDich = ChienDich::find()
                    ->andFilterWhere(['id' => $_POST['model']])
                    ->andFilterWhere(['active' => 1])
                    ->andFilterWhere(['trang_thai' => ChienDich::HOAT_DONG])
                    ->one();
                if (is_null($chienDich)) {
                    throw new HttpException(500, 'Chiến dịch không hợp lệ');
                } else {
                    $model = new Postback();
                    $model->utm_source = time();//$_POST['user']['uid'] . time().$chienDich->id;
                    $model->utm_medium = date("dHis");
                    $model->status = 0;
                    $model->created = date('Y-m-d H:i:s');
                    $model->user_id = $_POST['user']['uid'];
                    $model->chien_dich_id = $chienDich->id;
                    $model->trang_thai = ChienDich::KHOI_TAO;
                    if (!$model->save()) {
                        throw new HttpException(500, Html::errorSummary($model));
                    } else {
                        /** @var User $user */
                        $user = User::find()
                            ->andFilterWhere(['id' => $model->user_id])
                            ->select(['id', 'username', 'hoten', 'auth_key', 'total'])
                            ->one();
                        return [
                            'success' => 'success',
                            'type' => 'success',
                            'content' => 'Cảm ơn bạn đã tham gia chiến dịch này.',
                            'url' => "{$chienDich->track_link}&utm_source={$model->utm_source}&utm_medium={$model->utm_medium}",
//              'url' => "{$chienDich->track_link}&utm_source={$model->utm_source}",
                            'user' => [
                                'uid' => $user->id,
                                'auth' => $user->auth_key,
                                'ho_ten' => $user->hoten,
                                'dien_thoai' => $user->dien_thoai,
                                'total' => $user->total,
                                'anhdaidien' => $user->anhdaidien,
                            ],
                        ];
                    }
                }
            }
        }
    }

    //postback
    public function actionPostback($utm_source, $status, $reward)
    {
        $postback = Postback::findOne(['utm_source' => $utm_source]);
        if (is_null($postback)) {
            $postback = new Postback();
            $postback->utm_source = $utm_source;
            $postback->created = date('Y-m-d H:i:s');
        }
        $postback->status = $status;
        $postback->reward = $reward;
        if (strpos($utm_source, '_')) {
            $uid_chien_dich_id = explode('_', $utm_source);
            $postback->user_id = isset($uid_chien_dich_id[0]) ? $uid_chien_dich_id[0] : null;
            $postback->chien_dich_id = isset($uid_chien_dich_id[1]) ? $uid_chien_dich_id[1] : null;
        }
        $postback->updated = date('Y-m-d H:i:s');
        if (!$postback->save()) {
            throw new HttpException(500, Html::errorSummary($postback));
        }

        return $status;
    }

    /** app-blog */
    public function actionAppBlog()
    {
        $chienDichs = ChienDich::find()
            ->andWhere(['active' => 1, 'trang_thai' => ChienDich::HOAT_DONG])
            ->andWhere(['danh_muc_id' => 73])
            ->orderBy(['vi_tri_hien_thi' => SORT_ASC])
            ->limit(6);
        if (isset($_POST['search']))
            if ($_POST['search'] != '')
                $chienDichs->andFilterWhere(['like','title', $_POST['search']]);
        if (isset($_POST['page'])) {
            $chienDichs->offset(intval($_POST['page']) * 6);
        }
        $chienDichs = $chienDichs->all();
        return [
            'chienDichs' => $chienDichs,
            'count' => count($chienDichs),
        ];
    }

    /** app-category */
    public function actionAppCategory()
    {
        $dichVus = DanhMuc::find()
            ->select(['id', 'image', 'name', 'ti_le_hoan_tien'])
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['type' => $_POST['type']])
            ->orderBy(['vi_tri' => SORT_ASC]);
        if (isset($_POST['page'])) {
            $dichVus->offset(intval($_POST['page']) * 6);
        }
        $dichVus = $dichVus->all();
        return [
            'services' => $dichVus,
            'count' => count($dichVus),
        ];
    }

    /** redirect-link */
    public function actionRedirectLink($chien_dich_id)
    {
        if (isset(Yii::$app->session['uid'])) {
            $uid = Yii::$app->session->get('uid');
            $old_postback = Postback::findOne(['utm_source' => $uid . '_' . $chien_dich_id]);
            if (is_null($old_postback)) {
                $postback = new Postback();
                $postback->utm_source = $uid . '_' . $chien_dich_id;
                $postback->status = 0;
                $postback->created = date('Y-m-d H:i:s');
                $postback->user_id = $uid;
                $postback->chien_dich_id = $chien_dich_id;
                $postback->trang_thai = ChienDich::KHOI_TAO;
                if (!$postback->save()) {
                    throw new HttpException(500, Html::errorSummary($postback));
                } else {
                    return $this->redirect("{$postback->chienDich->track_link}?utm_source={$uid}_{$chien_dich_id}");
                }
            } else {
                if ($old_postback->trang_thai == ChienDich::KHOI_TAO) {
                    return $this->redirect("{$old_postback->chienDich->track_link}?utm_source={$uid}_{$chien_dich_id}");
                } else {
                    return $this->redirect('https://app.vietlixi.com');
                }
            }
        } else {
            return $this->redirect('https://app.vietlixi.com');
        }
    }

    /** app-transaction-history */
    public function actionAppTransactionHistory()
    {
        if (isset($_POST['user'])) {
            $chien_dich_postbacks = ChienDichPostback::find()
                ->andWhere(['active' => 1])
                ->orderBy(['ngay_thuc_hien' => SORT_DESC])
                ->limit(6);
            if (isset($_POST['page'])) {
                $chien_dich_postbacks->offset(intval($_POST['page']) * 6);
            }
            if (!User::isViewAll($_POST['user']['uid'])) {
                $chien_dich_postbacks->andWhere(['nguoi_thuc_hien_id' => $_POST['user']['uid']]);
            }
            $transactions = $chien_dich_postbacks->all();
            return [
                'transactions' => $transactions,
                'count' => count($transactions),
            ];
        } else {
            throw new HttpException(500, 'Không xác thưc được dữ liệu');
        }
    }

    /** transaction-detail */
    public function actionTransactionDetail()
    {
        if (isset($_POST['user'])) {
            $chien_dich_postback = ChienDichPostback::find()
                ->andWhere(['id' => $_POST['id']])
                ->one();
            return [
                'model' => $chien_dich_postback,
                'router' => 'app-transaction-detail.html',
            ];
        } else {
            throw new HttpException(500, 'Không xác thưc được dữ liệu');
        }
    }

    /** transaction-detail */
    public function actionAppHistory()
    {
        if (isset($_POST['user'])) {
            $lich_su_giao_dichs = LichSuGiaoDich::find();
            if (!User::isViewAll($_POST['user']['uid'])) {
                $lich_su_giao_dichs->andWhere(['user_id' => $_POST['user']['uid']]);
            }
            if (isset($_POST['page'])) {
                $lich_su_giao_dichs->offset(intval($_POST['page']) * 6);
            }
            $lich_su_giao_dichs = $lich_su_giao_dichs->all();

            return [
                'model' => $lich_su_giao_dichs,
                'count' => count($lich_su_giao_dichs),
            ];
        } else {
            throw new HttpException(500, 'Không xác thưc được dữ liệu');
        }
    }

    /** lich-su-rut-tien */
    public function actionLichSuRutTien()
    {
        if (isset($_POST['user'])) {
            $cho_duyets = LichSuGiaoDich::find()->andWhere(['type' => LichSuGiaoDich::RUT_TIEN, 'trang_thai' => LichSuGiaoDich::CHO_DUYET]);
            if (!User::isViewAll($_POST['user']['uid'])) {
                $cho_duyets->andWhere(['user_id' => $_POST['user']['uid']]);
            }
            $cho_duyets = $cho_duyets->all();

            $lich_sus = LichSuGiaoDich::find()->andWhere(['type' => LichSuGiaoDich::RUT_TIEN])->andWhere(['<>', 'trang_thai', LichSuGiaoDich::CHO_DUYET]);
            if (!User::isViewAll($_POST['user']['uid'])) {
                $lich_sus->andWhere(['user_id' => $_POST['user']['uid']]);
            }
            $lich_sus = $lich_sus->all();

            return [
                'lichSus' => $lich_sus,
                'choDuyets' => $cho_duyets,
            ];
        } else {
            throw new HttpException(500, 'Không xác thực dữ liệu');
        }
    }

    /** update-campain */
    public function actionUpdateCampain()
    {
        $arr = [];
        $stt = 1;
        $url = 'https://api.accesstrade.vn/v1/campaigns';
        $collection_name = "approval=successful";
        $request_url = $url . '?' . $collection_name;
        $curl = curl_init($request_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization: Token jH2DxLW7DxFQC_lMkvix471eYC_o-UQz',
            'Content-Type: application/json',
        ]);
        $response = curl_exec($curl);
        curl_close($curl);

        $data = json_decode($response);

        foreach ($data->data as $campain) {
            $arr[] = $campain->id;
        }

        if ($stt < intval($data->total_page)) {
            $page = $stt + 1;
            $url = 'https://api.accesstrade.vn/v1/campaigns';
            $collection_name = "approval=successful&page={$page}";
            $request_url = $url . '?' . $collection_name;
            $curl = curl_init($request_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Authorization: Token jH2DxLW7DxFQC_lMkvix471eYC_o-UQz',
                'Content-Type: application/json',
            ]);
            $response = curl_exec($curl);
            curl_close($curl);

            $data = json_decode($response);

            foreach ($data->data as $campain) {
                $arr[] = $campain->id;
            }
            $stt++;
        }
        $chien_dichs = ChienDich::find()->select('id')->andWhere(['not in', 'ma_chien_dich', $arr]);
        foreach ($chien_dichs as $chien_dich) {
            $postbacks = Postback::find()->andWhere(['trang_thai' => ChienDich::HOAT_DONG, 'chien_dich_id' => $chien_dich->id])
                ->andWhere(['>=', 'updated', date('Y-m-d H:i:s', strtotime('- 7 days'))])->all();
            foreach ($postbacks as $postback) {
                $postback->updateAttributes(['trang_thai' => ChienDich::KHONG_HOAT_DONG]);
                if ($postback->user) {
                    $postback->user->updateAttributes(['total' => ($postback->user->total - $postback->reward)]);
                }
            }
        }
    }

    //get-bao-cao-don-hang
    public function actionGetBaoCaoDonHang()
    {
        $user =     User::findOne([
            'auth_key' => $_POST['user']['auth'],
            'id' => $_POST['user']['uid'],
            'status' => 10
        ]);

        if (is_null($user))
            return [
                'message' => 'Vui lòng đăng nhập',
                'data' => []
            ];
        else {
            $baoCaoDonHang = QuanLyPostback::find()
                ->select(['id', 'title', 'so_tien_nhan_duoc', 'hoten', 'trang_thai', 'image',
                    'chi_phi_chien_dich'])
                ->andFilterWhere([
                    'active' => 1,
                    'user_id' => $user->id,
                ])
                ->orderBy(['id' => SORT_DESC])
                ->all();
            return [
                'message' => '',
                'data' => $baoCaoDonHang
            ];
        }
    }

    //postback-detail
    public function actionPostbackDetail()
    {
        $user = User::findOne([
            'id' => $_POST['user']['uid'],
            'auth_key' => $_POST['user']['auth']
        ]);
        if (is_null($user))
            throw new HttpException(500, 'Vui lòng đăng nhập');
        else {
            $model = QuanLyPostback::find()
                ->select(['id', 'title', 'so_tien_nhan_duoc', 'hoten', 'trang_thai', 'image',
                    'chi_phi_chien_dich'])
                ->andFilterWhere(['id' => $_POST['postback']])
                ->andFilterWhere(['active' => 1])
                ->one();
            $trangThaiPostBack = [];
            if (!is_null($model))
                $trangThaiPostBack = TrangThaiChienDich::find()
                    ->andFilterWhere(['postback_id' => $model->id])
                    ->all();
            return [
                'model' => $model,
                'trangThai' => $trangThaiPostBack
            ];
        }
    }

    /**
     * @param $type string
     * @param $trangThai string
     * @param $user User
     * @return array
     * @throws \yii\db\Exception
     */
    public function getThongKeVi($type, $trangThai, $user)
    {
        $tong = 0;
        $soLuong = 0;
        $ngayGanNhat = 'Đang cập nhật';
        $soTienGanNhat = 0;

        if ($type != LichSuGiaoDich::DOI_QUA){
            $sql = "SELECT SUM(cost) as tong, Count(id) as so_luong FROM tc_lich_su_giao_dich
                WHERE user_id = :id and trang_thai = :tt and active = 1 and type = :type";
            $query = Yii::$app->db->createCommand($sql, [':id' => $user->id, ':tt' => $trangThai, ':type' => $type])->queryAll();
            if (count($query) > 0) {
                $tong = $query[0]['tong'];
                $soLuong = $query[0]['so_luong'];
            }

            /** @var LichSuGiaoDich $daDuyet */
            $daDuyet = LichSuGiaoDich::find()
                ->andFilterWhere(['user_id' => $user->id, 'trang_thai' => $trangThai, 'type' => $type, 'active' => 1])
                ->orderBy(['id' => SORT_DESC])
                ->one();
            if (!is_null($daDuyet)) {
                /** @var TrangThaiGiaoDich $lanDuyetGanNhat */
                $lanDuyetGanNhat = TrangThaiGiaoDich::find()
                    ->andFilterWhere([
                        'giao_dich_id' => $daDuyet->id,
                        'trang_thai' => $trangThai])
                    ->orderBy(['id' => SORT_DESC])
                    ->one();
                if (!is_null($lanDuyetGanNhat)) {
                    $ngayGanNhat = date("d/m/Y", strtotime($lanDuyetGanNhat->created));
                    $soTienGanNhat = $daDuyet->cost;
                }
            }
        } else {
            $query = QuanLyQuaTangThanhVien::find()->andFilterWhere(['trang_thai' => LichSuGiaoDich::DA_DUYET, 'user_id' => $user->id])->all();

            if (count($query) > 0) {
                $tong = $query[0]['tong_tien'];
                $soLuong = $query[0]['so_luong'];
            }

            /** @var QuanLyQuaTangThanhVien $daDuyet */
            $daDuyet = QuanLyQuaTangThanhVien::find()
                ->andFilterWhere(['user_id' => $user->id, 'trang_thai' => $trangThai, 'active' => 1])
                ->orderBy(['id' => SORT_DESC])
                ->one();

            if (!is_null($daDuyet)) {
                /** @var TrangThaiQuaTangThanhVien $lanDuyetGanNhat */
                $lanDuyetGanNhat = TrangThaiQuaTangThanhVien::find()
                    ->andFilterWhere([
                        'qua_tang_thanh_vien_id' => $daDuyet->id,
                        'trang_thai' => $trangThai])
                    ->orderBy(['id' => SORT_DESC])
                    ->one();
                if (!is_null($lanDuyetGanNhat)) {
                    $ngayGanNhat = date("d/m/Y", strtotime($lanDuyetGanNhat->created));
                    $soTienGanNhat = $daDuyet->tong_tien;
                }
            }
        }

        //        $tong = 0;
        //        $soLuong = 0;
        //        $ngayGanNhat = '';
        //        $soTienGanNhat = 0;
        return [
            'tongTien' => $tong,
            'soLuong' => $soLuong,
            'ngayGanNhat' => $ngayGanNhat,
            'soTienGanNhat' => $soTienGanNhat
        ];
    }

    //tong-hop-vi-cua-toi
    public function actionTongHopViCuaToi()
    {
        //Tổng đã duyệt
        if (isset($_POST['user']['uid']) && isset($_POST['user']['auth'])) {
            $user = User::findOne([
                'id' => $_POST['user']['uid'],
                'auth_key' => $_POST['user']['auth'],
                'status' => 10
            ]);
            if (is_null($user))
                throw new HttpException(500, 'Vui lòng đăng nhập');
            else {
                return [
                    'DaDuyet' => self::getThongKeVi(LichSuGiaoDich::RUT_TIEN, LichSuGiaoDich::DA_DUYET, $user),
                    'DaHuy' => self::getThongKeVi(LichSuGiaoDich::RUT_TIEN, LichSuGiaoDich::DA_HUY, $user),
                    'ChoDuyet' => self::getThongKeVi(LichSuGiaoDich::RUT_TIEN, LichSuGiaoDich::CHO_DUYET, $user),
                    'DaRut' => self::getThongKeVi(LichSuGiaoDich::RUT_TIEN, LichSuGiaoDich::DA_DUYET, $user),
                    'DoiQua' => self::getThongKeVi(LichSuGiaoDich::DOI_QUA, LichSuGiaoDich::DA_DUYET, $user),
                ];
            }
        } else
            throw new HttpException(500, "Vui lòng đăng nhập");
//        $sql = "SELECT Sum() From tc_lich_su_giao_dich where "
    }

    //chi-tiet-giao-dich
    public function actionChiTietGiaoDich()
    {
        if (isset($_POST['user']['uid']) && isset($_POST['user']['auth']) && isset($_POST['type'])) {
            $user = User::findOne([
                'id' => $_POST['user']['uid'],
                'auth_key' => $_POST['user']['auth'],
                'status' => 10
            ]);
            if (is_null($user))
                throw new HttpException(500, 'Vui lòng đăng nhập');
            else {
                if (!in_array($_POST['type'], ['Đã duyệt', 'Đã hủy', 'Chờ duyệt', 'Đổi quà', 'Đã rút'])) {
                    throw new HttpException(500, 'Dữ liệu không phù hợp');
                } else {
                    if (in_array($_POST['type'], ['Đã duyệt', 'Đã hủy', 'Chờ duyệt'])) {
                        $type = LichSuGiaoDich::HOA_HONG;
                        $trangThai = $_POST['type'];
                        if ($_POST['type'] == 'Đã duyệt') {
                            $icon = 'add';
                            $color = 'success';
                        } else if ($_POST['type'] == 'Đã hủy') {
                            $icon = 'remove';
                            $color = 'danger';
                        } else {
                            $icon = 'refresh';
                            $color = 'warning';
                        }

                    } else {
                        $type = $_POST['type'];
                        $trangThai = LichSuGiaoDich::DA_DUYET;
                        if ($type == 'Đổi quà') {
                            $icon = 'cube';
                            $color = 'success';
                        } else {
                            $icon = "log-out";
                            $color = 'danger';
                        }
                    }
                    // Tính tổng tiền theo từng ngày
                    $sql = "SELECT date(created) as ngay_cap_nhat, sum(cost) as tong_tien FROM tc_lich_su_giao_dich 
                                WHERE type = :t and trang_thai = :tt and active = 1 
                                group by date(created)
                                order by date(created) DESC";
                    $data = Yii::$app->db->createCommand($sql, [
                        ':t' => $type,
                        ':tt' => $trangThai
                    ])->queryAll();

                    $results = [];
                    foreach ($data as $item) {
                        $results[$item['ngay_cap_nhat']] = [
                            'ngay' => date("d/m/Y", strtotime($item['ngay_cap_nhat'])),
                            'data' => Yii::$app->db->createCommand(
                                "select t2.title, t2.image, t.id, t.cost, t3.hoten as nguoi_duyet
                                    from tc_lich_su_giao_dich t left join tc_chien_dich t2 on t2.id = t.chien_dich_post_back_id
                                    left join tc_user t3 on t3.id = t.nguoi_duyet_id
                                    where date(t.created) = :t and t.type = :type and t.trang_thai = :tt and t.user_id = :uid"
                                , [
                                ':t' => $item['ngay_cap_nhat'],
                                ':type' => $type,
                                ':tt' => $trangThai,
                                ':uid' => $user->id
                            ])->queryAll()
                        ];
                    }

                    return [
                        'tongNgay' => $data,
                        'color' => $color,
                        'icon' => $icon,
                        'results' => $results
                    ];
                }
            }
        } else
            throw new HttpException(500, "Vui lòng đăng nhập");
    }

    //get-gifts
    public function actionGetGifts()
    {
        return QuaTang::find()->select(['id', 'name', 'gia_tri', 'anh_dai_dien'])
            ->andFilterWhere(['active' => 1])
            ->limit(10)
            ->offset(0)
            ->orderBy(['vi_tri' => SORT_ASC])
            ->all();
    }

    //nhan-qua
    public function actionNhanQua()
    {
        $time = 10;
        $contentFailed = 'Vui lòng đăng nhập!<br/>Bạn sẽ được chuyển tới trang <a href="https://vietlixi.com/app-login.html" title="Đăng nhập hệ thống">đăng nhập</a> sau <span id="thoi-gian">' . $time . '</span> giây hoặc hãy nhấn vào <a href="https://vietlixi.com/app-login.html" title="Đăng nhập hệ thống">đây</a> để chuyển trang.';
        $arrFailed = [
            'status' => 'failed',
            'type' => 'logged',
            'content' => $contentFailed,
            'time' => $time,
        ];

        if (!isset($_POST['user'])) {
            return $arrFailed;
        } else {
            if ($_POST['user'] == '')
                return $arrFailed;
            else {
                $user = User::findOne([
                    'id' => $_POST['user']['uid'],
                    'auth_key' => $_POST['user']['auth'],
                    'status' => 10
                ]);
                if (is_null($user))
                    return $arrFailed;
                else {
                    return [
                        'status' => 'success',
                        'quaTang' => QuaTang::find()
                            ->select(['id', 'name', 'anh_dai_dien', 'gia_tri'])
                            ->andFilterWhere(['id' => $_POST['quaTang']])
                            ->one()
                    ];
                }
            }
        }
    }

    //nhan-doi-qua
    public function actionNhanDoiQua()
    {
        $arrFailed = [
            'status' => 'failed',
            'content' => 'Vui lòng đăng nhập',
        ];
        if (!isset($_POST['user']))
            return $arrFailed;
        else {
            $soLuong = ($_POST['soLuong'] == '' ? 0 : intval($_POST['soLuong']));
            if ($soLuong <= 0)
                return [
                    'status' => 'failed',
                    'content' => 'Vui lòng chọn ít nhất 1 sản phẩm'
                ];
            else {
                if (isset($_POST['user']['uid'])) {
                    /** @var User $user */
                    $user = User::find()
                        ->andFilterWhere(['id' => $_POST['user']['uid']])
                        ->andFilterWhere(['auth_key' => $_POST['user']['auth']])
                        ->andFilterWhere(['status' => 10])
                        ->one();
                    if (is_null($user))
                        return $arrFailed;
                    else {
                        $quaTang = QuaTang::findOne(['active' => 1, 'id' => $_POST['quaTang']]);
                        if (is_null($quaTang))
                            return [
                                'status' => 'failed',
                                'content' => 'Thông tin quà tặng không hợp lệ'
                            ];
                        else {
                            $tongTienDonHangCho = Yii::$app->db->createCommand('select sum(tong_tien) as tong_tien from tc_qua_tang_thanh_vien where khach_hang_id = :k and trang_thai = :tt and active = :hh', [
                                ':k' => $user->id,
                                ':tt' => QuaTangThanhVien::CHO_DUYET,
                                ':hh' => 1
                            ])->queryAll();
                            $tongTien = 0;
                            if (isset($tongTienDonHangCho[0]))
                                if (isset($tongTienDonHangCho[0]['tong_tien']))
                                    $tongTien = intval($tongTienDonHangCho[0]['tong_tien']);

                            if ($tongTien + $soLuong * $quaTang->gia_tri > $user->vi_dien_tu)
                                return [
                                    'status' => 'failed',
                                    'content' => 'Số tiền trong ví của bạn '
                                        . number_format($user->vi_dien_tu, 0, '', '.') . 'đ không đủ để thực hiện việc này (Tổng: '
                                        . number_format($tongTien + $soLuong * $quaTang->gia_tri, 0, '', '.') . 'đ)'
                                ];
                            else {
                                    $model = new QuaTangThanhVien();
                                    $model->qua_tang_id = $quaTang->id;
                                    $model->khach_hang_id = $user->id;
                                    $model->trang_thai = QuaTangThanhVien::CHO_DUYET;
                                    $model->created = date("Y-m-d H:i:s");
                                    $model->user_id = $user->id;
                                    $model->active = 1;
                                    $model->so_luong = $soLuong;
                                    $model->don_gia = $quaTang->gia_tri;
                                    $model->tong_tien = $model->don_gia * $model->so_luong;
                                    $user->updateAttributes(['pending_total' => $user->pending_total +$model->tong_tien]);

                                if ($model->save()) {
                                    return [
                                        'status' => 'success',
                                        'content' => 'Đã lưu thông tin quà tặng thành công. Bạn có thể truy cập chức năng <a href="https://vietlixi.com/thong-ke-doi-qua.html" title="Đổi quà">Thống kê đổi quà</a> để xem chi tiết'
                                    ];
                                } else {
                                    return [
                                        'status' => 'failed',
                                        'content' => Html::errorSummary($model)
                                    ];
                                }
                            }
                        }
                    }
                } else
                    return $arrFailed;
            }
        }
    }

    //nhan-rut-tien
    public function actionNhanRutTien()
    {
        $arrFailed = [
            'status' => 'failed',
            'content' => 'Vui lòng đăng nhập',
        ];
        if (!isset($_POST['user']))
            return $arrFailed;
        else {
            $soTien = ($_POST['soTien'] == '' ? 0 : intval($_POST['soTien']));
            if ($soTien <= 0)
                return [
                    'status' => 'failed',
                    'content' => 'Vui lòng nhập số tiền muốn rút'
                ];
            else {
                if (isset($_POST['user']['uid'])) {
                    /** @var User $user */
                    $user = User::find()
                        ->andFilterWhere(['id' => $_POST['user']['uid']])
                        ->andFilterWhere(['auth_key' => $_POST['user']['auth']])
                        ->andFilterWhere(['status' => 10])
                        ->one();
                    if (is_null($user))
                        return $arrFailed;
                    else {
                        $tongTienUser = $user->total;
                        $soTien = explode(',', $_POST['soTien']);
                        $soTien = implode('', $soTien);
                        if ($soTien > $tongTienUser)
                            return [
                                'status' => 'failed',
                                'content' => 'Số dư trong ví của bạn không đủ để thực hiện hành động này.'
                            ];
                        else {
                                $model = new LichSuGiaoDich();

                                $model->cost = $soTien;
                                $model->bank_number = $_POST['soTaiKhoan'];
                                $model->bank_name = $_POST['tenNganHang'];
                                $model->type = 'Rút tiền';
                                $model->created = date('Y-m-d');
                                $model->username = $user->username;
                                $model->user_id = $user->id;
                                $user->updateAttributes(['pending_total' => $user->pending_total +$model->cost]);

                                if ($model->save()) {
                                    return [
                                        'status' => 'success',
                                        'content' => 'Đã lưu thông tin giao dịch thành công. Bạn có thể truy cập chức năng <a href="https://vietlixi.com/thong-ke-rut-tien.html" title="Rút tiền">Thống kê rút tiền</a> để xem chi tiết'
                                    ];
                                } else {
                                    return [
                                        'status' => 'failed',
                                        'content' => Html::errorSummary($model)
                                    ];
                                }
                            }
                    }
                } else
                    return $arrFailed;
            }
        }
    }

    //get-thong-ke-doi-qua
    public function actionGetThongKeDoiQua()
    {
        $trangThai = [
            QuaTangThanhVien::CHO_DUYET => QuaTangThanhVien::CHO_DUYET,
            QuaTangThanhVien::DA_DUYET => QuaTangThanhVien::DA_DUYET,
            QuaTangThanhVien::HUY => QuaTangThanhVien::HUY
        ];
        foreach ($trangThai as $item) {
            $trangThai[$item] = QuanLyQuaTangThanhVien::find()
                ->select(['id', 'name', 'don_gia', 'so_luong', 'created',
                    'tong_tien', 'trang_thai', 'anh_dai_dien'])
                ->andFilterWhere([
                    'trang_thai' => $item,
                    'khach_hang_id' => $_POST['user']['uid']
                ])
                ->orderBy(['id' => SORT_DESC])
                ->limit(5)
                ->all();
        }

        return $trangThai;
    }

    //get-thong-ke-doi-qua
    public function actionGetThongKeRutTien()
    {
        $trangThai = [
            LichSuGiaoDich::CHO_DUYET => LichSuGiaoDich::CHO_DUYET,
            LichSuGiaoDich::DA_DUYET => LichSuGiaoDich::DA_DUYET,
            LichSuGiaoDich::DA_HUY => LichSuGiaoDich::DA_HUY
        ];
        foreach ($trangThai as $item) {
            $trangThai[$item] = LichSuGiaoDich::find()
                ->select(['id', 'chien_dich_post_back_id', 'cost', 'created', 'updated'
                , 'ghi_chu', 'username', 'nguoi_duyet_id', 'bank_number', 'bank_name'])
                ->andFilterWhere([
                    'trang_thai' => $item,
                    'type' => LichSuGiaoDich::RUT_TIEN,
                    'user_id' => $_POST['user']['uid']
                ])
                ->orderBy(['id' => SORT_DESC])
                ->limit(5)
                ->all();
        }

        return $trangThai;
    }

    //Huy nhan qua
    public function actionHuyNhanQua(){
        $qua_tang = QuaTangThanhVien::findOne(['id' => $_POST['quaTang']]);
        if (is_null($qua_tang)){
            throw new HttpException(500, 'Thông tin quà tặng không hợp lệ hoặc đã bị xoá');
        }
        if ($qua_tang->trang_thai != QuaTangThanhVien::DA_DUYET || $qua_tang->trang_thai != QuaTangThanhVien::CHO_DUYET){
            $qua_tang->updateAttributes(['trang_thai'=> QuaTangThanhVien::HUY]);
            return [
                'message' => 'Hủy yêu cầu nhận quà thành công'
            ];
        }
    }

    //Huy rút tiền
    public function actionHuyRutTien(){
        $qua_tang = LichSuGiaoDich::findOne(['id' => $_POST['rutTien']]);
        if (is_null($qua_tang)){
            throw new HttpException(500, 'Thông tin quà tặng không hợp lệ hoặc đã bị xoá');
        }
        if ($qua_tang->trang_thai != LichSuGiaoDich::DA_DUYET || $qua_tang->trang_thai != LichSuGiaoDich::CHO_DUYET){
            $qua_tang->updateAttributes(['trang_thai'=> LichSuGiaoDich::DA_HUY]);
            return [
                'message' => 'Hủy yêu cầu rút tiền thành công'
            ];
        }
    }

    //Update qua tang
    public function actionUpdateQuaTang(){

        $quaTang = QuaTangThanhVien::findOne($_POST['quaTang']);
        if (is_null($quaTang))
            throw new HttpException(500, "Thông tin quà tặng không hợp lệ");
        $user = User::findOne($_POST['user']['uid']);
        $tongTienDonHangCho = Yii::$app->db->createCommand('select sum(tong_tien) as tong_tien from tc_qua_tang_thanh_vien where khach_hang_id = :k and trang_thai = :tt and active = :hh', [
            ':k' => $user->id,
            ':tt' => QuaTangThanhVien::CHO_DUYET,
            ':hh' => 1
        ])->queryAll();
        $tongTien = 0;
        if (isset($tongTienDonHangCho[0]))
            if (isset($tongTienDonHangCho[0]['tong_tien']))
                $tongTien = intval($tongTienDonHangCho[0]['tong_tien']);

        if ($tongTien + $_POST['soLuong'] * $quaTang->don_gia > $user->vi_dien_tu)
            return [
                'status' => 'failed',
                'content' => 'Số tiền trong ví của bạn '
                    . number_format($user->vi_dien_tu, 0, '', '.') . 'đ không đủ để thực hiện việc này (Tổng: '
                    . number_format($tongTien + $_POST['soLuong'] * $quaTang->don_gia, 0, '', '.') . 'đ)'
            ];

        $tongTienNew = $_POST['soLuong'] * $quaTang->don_gia;
        $quaTang->updateAttributes(['so_luong' => $_POST['soLuong']]);
        $quaTang->updateAttributes(['tong_tien' => $tongTienNew]);

        return [
            'status' => 'success',
            'content' => 'Cập nhật giao dịch thành công'
        ];
    }
    //Update qua tang
    public function actionUpdateRutTien(){

        $rut_tien = LichSuGiaoDich::findOne($_POST['rutTien']);
        if (is_null($rut_tien))
            throw new HttpException(500, "Thông tin giao dịch không hợp lệ");
        $user = User::findOne($_POST['user']['uid']);
        $tongTienDonHangCho = Yii::$app->db->createCommand('select sum(cost) as tong_tien from tc_lich_su_giao_dich where user_id = :k and trang_thai = :tt and active = :hh', [
            ':k' => $user->id,
            ':tt' => LichSuGiaoDich::CHO_DUYET,
            ':hh' => 1
        ])->queryAll();
        $tongTien = 0;
        if (isset($tongTienDonHangCho[0]))
            if (isset($tongTienDonHangCho[0]['tong_tien']))
                $tongTien = intval($tongTienDonHangCho[0]['tong_tien']);

        if ($tongTien + $rut_tien->cost > $user->vi_dien_tu)
            return [
                'status' => 'failed',
                'content' => 'Số tiền trong ví của bạn '
                    . number_format($user->vi_dien_tu, 0, '', '.') . 'đ không đủ để thực hiện việc này (Tổng: '
                    . number_format($tongTien + $rut_tien->cost, 0, '', '.') . 'đ)'
            ];

        $soTienUpdate = explode('.', $_POST['soTien']);
        $soTienUpdate = implode('', $soTienUpdate);

        $rut_tien->updateAttributes(['bank_name' => $_POST['tenNganHang']]);
        $rut_tien->updateAttributes(['cost' => $soTienUpdate]);
        $rut_tien->updateAttributes(['bank_number' => $_POST['soTaiKhoan']]);

        return [
            'status' => 'success',
            'content' => 'Cập nhật giao dịch thành công'
        ];
    }

    //load-qua-tang-for-update
    public function actionLoadQuaTangForUpdate()
    {
        /** @var QuanLyQuaTangThanhVien $quaTangKhachHang */
        $quaTangKhachHang = QuanLyQuaTangThanhVien::find()
            ->select(['id', 'qua_tang_id', 'khach_hang_id', 'trang_thai', 'so_luong', 'don_gia', 'name', 'anh_dai_dien', 'created', 'tong_tien'])
            ->andFilterWhere(
                [
                    'id' => $_POST['quaTang'],
                    'khach_hang_id' => $_POST['user']['uid'],
                    'active' => 1
                ]
            )->one();
        if (is_null($quaTangKhachHang))
            throw new HttpException(500, 'Thông tin quà tặng không hợp lệ hoặc đã bị xoá');
        else {
            if ($quaTangKhachHang->trang_thai != QuaTangThanhVien::CHO_DUYET)
                throw new HttpException(500, 'Yêu cầu nhận quà của bạn đã được duyệt hoặc bị huỷ bởi quản lý, vui lòng cập nhật lại danh sách thống kê quà tặng này.');
            else {
                return [
                    'quaTang' => $quaTangKhachHang
                ];
            }
        }
    }

    //load-rut-tien-for-update
    public function actionLoadRutTienForUpdate(){
        $rutTien = LichSuGiaoDich::findOne([
            'id' => $_POST['rutTien'],
            'user_id' => $_POST['user']['uid'],
            'active' => 1
        ]);

        if (is_null($rutTien))
            throw new HttpException(500, 'Thông tin giao dịch không hợp lệ hoặc bị xóa');

        if ($rutTien->trang_thai != LichSuGiaoDich::CHO_DUYET){
            throw new HttpException(500, "Yêu cầu rút tiền của bạn đã được duyệt hoặc bị hủy bởi quản lý, vui lòng cập nhật lại danh sách thống kê rút tiền này.");
        }

        return [
            'rutTien' => $rutTien
        ];
    }
    //cap nhat hoa hong chien dich
    public function actionCapNhatHoaHongChienDich(){
        //Kiểm tra ngày hôm nay đã update hoa hồng chưa
        $dongBo = TiLeChiaSeHoaHong::find()
            ->andWhere('date(created) = :today', [':today' => date('Y-m-d')])
            ->one();

        $chienDichDaCapNhat = 0;
        $chienDich = ChienDich::find()->andFilterWhere(['active' => 1])->all();

        /** @var ChienDich $item */
        foreach ($chienDich as $item){
            if ($item->ma_chien_dich == ''){
                continue;
            }

            $campId = (string)$item->ma_chien_dich;
            $montYear = date('m-Y');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.accesstrade.vn/v1/commission_policies?camp_id='.$campId.'&month='.$montYear,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization:  Token jH2DxLW7DxFQC_lMkvix471eYC_o-UQz',
                    'Content-Type:  application/json'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response);


            if (count($data->category)>0){
                foreach ($data->category as $category){
                    if (is_null($dongBo)){
                        $ti_le_hoa_hong_category = new TiLeChiaSeHoaHong();
                    } else {
                        $ti_le_hoa_hong_category = TiLeChiaSeHoaHong::find()->andFilterWhere(['ma_chien_dich' => $item->ma_chien_dich, 'type' => TiLeChiaSeHoaHong::_CATEGORY])->all();
                    }
                    $ti_le_hoa_hong_category->ma_chien_dich = $item->ma_chien_dich;
                    $ti_le_hoa_hong_category->type = TiLeChiaSeHoaHong::_CATEGORY;
                    $ti_le_hoa_hong_category->category_name = $category->category_name;
                    $ti_le_hoa_hong_category->customer_type = $category->customer_type;
                    $ti_le_hoa_hong_category->sales_price = $category->sales_price;
                    $ti_le_hoa_hong_category->sales_ratio = $category->sales_ratio;
                    $ti_le_hoa_hong_category->taget_month = $category->taget_month;
                    $ti_le_hoa_hong_category->save();
                }
            }
            if (count($data->default)>0){
                foreach ($data->default as $category){
                    if (is_null($dongBo)){
                        $ti_le_hoa_hong_default = new TiLeChiaSeHoaHong();
                    } else {
                        $ti_le_hoa_hong_default = TiLeChiaSeHoaHong::find()->andFilterWhere(['ma_chien_dich' => $item->ma_chien_dich, 'type' => TiLeChiaSeHoaHong::_DEFAULT])->all();
                    }
                    $ti_le_hoa_hong_default->ma_chien_dich = $item->ma_chien_dich;
                    $ti_le_hoa_hong_default->type = TiLeChiaSeHoaHong::_DEFAULT;
                    $ti_le_hoa_hong_default->customer_type = $category->customer_type;
                    $ti_le_hoa_hong_default->reward_type = $category->reward_type;
                    $ti_le_hoa_hong_default->sales_price = $category->sales_price;
                    $ti_le_hoa_hong_default->sales_ratio = $category->sales_ratio;
                    $ti_le_hoa_hong_default->taget_month = $category->taget_month;
                    $ti_le_hoa_hong_default->save();
                }
            }
            if (count($data->product)>0){
                foreach ($data->product as $category){
                    if (is_null($dongBo)){
                        $ti_le_hoa_hong_product = new TiLeChiaSeHoaHong();
                    } else {
                        $ti_le_hoa_hong_product = TiLeChiaSeHoaHong::find()->andFilterWhere(['ma_chien_dich' => $item->ma_chien_dich, 'type' => TiLeChiaSeHoaHong::_PRODUCT])->all();
                    }
                    $ti_le_hoa_hong_product->ma_chien_dich = $item->ma_chien_dich;
                    $ti_le_hoa_hong_product->type = TiLeChiaSeHoaHong::_PRODUCT;
                    $ti_le_hoa_hong_product->category_id = $category->category_id;
                    $ti_le_hoa_hong_product->sales_price = $category->sales_price;
                    $ti_le_hoa_hong_product->sales_ratio = $category->sales_ratio;
                    $ti_le_hoa_hong_product->taget_month = $category->taget_month;
                    $ti_le_hoa_hong_product->save();

                }
            }

            $chienDichDaCapNhat++;
        }

        return 'Đã cập nhật '.$chienDichDaCapNhat.' chiến dịch';
    }

//  // dong-bo-chien-dich
//  public function actionDongBoChienDich(){
//    $soLuongChienDichDaLuu = 0;
//
//    // Tìm xem có lệnh đồng bộ nào trong ngày không
//    $thoiGianDongBoHomNay = LichSuDongBoChienDich::find()
//      ->andWhere('date(thoi_gian) = :today', [':today' => date("Y-m-d")])
//      ->one();
//    $dongBo = true;
//    if(is_null($thoiGianDongBoHomNay)){
//      $url = 'https://api.accesstrade.vn/v1/campaigns';
//      $curl = curl_init($url);
//      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//      curl_setopt($curl, CURLOPT_HTTPHEADER, [
//        'Authorization: Token Cfo2pxU30zTWo1yVbBu-PJ2FqrXDS3Ty',
//        'Content-Type: application/json'
//      ]);
//      $response = curl_exec($curl);
//      curl_close($curl);
//
//      $data = json_decode($response);
//      for($i = 1; $i <= $data->total_page; $i++){
//        $model = new LichSuDongBoChienDich();
//        $model->thoi_gian = date("Y-m-d H:i:s");
//        $model->duong_dan_dong_bo = 'https://api.accesstrade.vn/v1/campaigns?page='.$i;
//        $model->save();
//      }
//      $dongBo = false;
//    }
//
//    if($dongBo){
//      $model = LichSuDongBoChienDich::find()
//        ->andWhere('da_dong_bo is null')
//        ->one();
//      if(!is_null($model)){
//        $danhMuc = ArrayHelper::map(
//          DanhMuc::find()->all(),
//          'name',
//          'id'
//        );
//        $model->updateAttributes(['da_dong_bo' => date("Y-m-d H:i:s")]);
//
//        $curl = curl_init($model->duong_dan_dong_bo);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, [
//          'Authorization: Token jH2DxLW7DxFQC_lMkvix471eYC_o-UQz',
//          'Content-Type: application/json'
//        ]);
//        $response = curl_exec($curl);
//        curl_close($curl);
//
//        $data = json_decode($response);
//
//        foreach ($data->data as $campain) {
//          $chien_dich = ChienDich::findOne(['ma_chien_dich' => $campain->id]);
//          if(is_null($chien_dich))
//            $chien_dich = new ChienDich();
//
//          if (!isset($danhMuc[$campain->category])) {
//            $category = new DanhMuc();
//            $category->type = DanhMuc::CATEGORY;
//            $category->name = $campain->category;
//            $category->save();
//            $danhMuc[$campain->category] = $category->id;
//          }
//          $chien_dich->category_id = $danhMuc[$campain->category];
//
//          $chien_dich->ma_chien_dich = $campain->id;
//          $chien_dich->conversion_requirements = $campain->description->action_point;
//          $chien_dich->image = $campain->logo;
//          $chien_dich->title = $campain->name;
//          $chien_dich->active = $campain->status;
//          $chien_dich->track_link = $campain->url;
//          if (strpos($campain->max_com, 'VNĐ')) {
//            $max_com =str_replace('VNĐ', '', $campain->max_com);
//            $max_com =str_replace(',', '', $max_com);
//            $chien_dich->hoa_hong_tu_net = doubleval(trim($max_com));
//          }
//          if (strpos($campain->max_com, '%')) {
//            $max_com =str_replace('%', '', $campain->max_com);
//            $max_com =str_replace(',', '.', $max_com);
//            $chien_dich->hoa_hong_tu_net = doubleval(trim($max_com));
//          }
//          $chien_dich->created = date('Y-m-d H:i:s');
//          $chien_dich->user_id = 1;
//          if($campain->approval == 'pending')
//            $chien_dich->trang_thai = ChienDich::CHO_DUYET;
//          else if($campain->approval == 'successful')
//            $chien_dich->trang_thai = ChienDich::HOAT_DONG;
//          else
//            $chien_dich->trang_thai = ChienDich::KHONG_HOAT_DONG;
//
//          if($chien_dich->save())
//            $soLuongChienDichDaLuu++;
//        }
//      }
//    }
//
//    return "Đã lưu {$soLuongChienDichDaLuu} chiến dịch";
//  }

    // get-profile
    public function actionGetProfile()
    {
        /** @var User $user */
        $user = User::find()
            ->select(['id', 'username', 'anhdaidien', 'ngay_sinh',
                'email', 'dien_thoai', 'link', 'hoten', 'ma_gioi_thieu'])
            ->andFilterWhere(['id' => $_POST['uid']])
            ->one();
        if (is_null($user))
            throw new HttpException(500, 'Không tồn tại thông tin người dùng');
        else
            return [
                'user' => $user
            ];
    }

    // save-profile
    public function actionSaveProfile()
    {
        /** @var User $existUser */
        $existUser = User::find()
            ->andFilterWhere(['dien_thoai' => $_POST['phoneNumber'], 'status' => 10])
            ->andWhere('id <> :i', [':i' => $_POST['uid']])
            ->one();
        if (!is_null($existUser))
            throw new HttpException(500, 'Số điện thoại đã tồn tại');
        else {
            $user = User::findOne($_POST['uid']);
            if ($_POST['currentPassword'] != '') {
                if (!Yii::$app->security->validatePassword($_POST['currentPassword'], $user->password_hash))
                    throw new HttpException(500, 'Mật khẩu hiện tại không chính xác');
                else {
                    if ($_POST['newPassword'] != $_POST['reNewPassword'])
                        throw new HttpException(500, 'Mật khẩu nhập lại không khớp');
                    else {
                        if (strlen($_POST['newPassword']) < 6)
                            throw new HttpException(500, 'Độ dài mật khẩu từ 6 ký tự trở lên');
                        else
                            $user->password_hash = Yii::$app->security->generatePasswordHash($_POST['newPassword']);
                    }
                }
            }
            if ($_POST['fullName'] != '')
                $user->hoten = $_POST['fullName'];
            else throw new HttpException(500, 'Vui lòng nhập họ tên');

            if ($_POST['email'] != '')
                $user->email = $_POST['email'];
            else throw new HttpException(500, 'Vui lòng nhập địa chỉ email');

            if ($_POST['phoneNumber'] != '')
                $user->dien_thoai = $_POST['phoneNumber'];
            else throw new HttpException(500, 'Vui lòng nhập số điện thoại');

            $user->username = $_POST['phoneNumber'];

            $file = UploadedFile::getInstanceByName('anhDaiDien');
            if (!is_null($file)) {
                $fileName = time() . myAPI::createCode($file->name);
                if (!is_null($file)) {
                    $file->saveAs(dirname(dirname(__DIR__)) . '/images/' . $fileName);
                    $user->anhdaidien = $fileName;
                }
            }

            if ($user->save())
                return [
                    'content' => 'Đã lưu thông tin cá nhân thành công'
                ];
            else
                throw new HttpException(500, Html::errorSummary($user));
        }
    }

    //update-postback
    public function actionUpdatePostback($utm_source, $status, $reward)
    {
        $model = Postback::findOne(['utm_source' => $utm_source]);
        $modelLichSuPB = new LichSuPostback();
        $modelLichSuPB->postback_id = $model->id;
        $modelLichSuPB->created = date("Y-m-d H:i:s");
        $modelLichSuPB->status = $status;
        $modelLichSuPB->reward = $reward;
        $modelLichSuPB->save();
        return 1;
    }

    public function actionGetBankName(){
        $bank_name = DanhMuc::find()->andFilterWhere([
            'active' => 1,
            'type' => DanhMuc::TEN_NGAN_HANG
        ])
        ->all();

        return [
          'ten_ngan_hang' => $bank_name,
        ];
    }

    //get thong ke nhan tien
    public function actionGetThongKeNhanTien(){
        $chienDich = ChienDichPostback::find()->andFilterWhere([
            'active' => 1,
            'user_id' => $_POST['user']['uid'],
            'trang_thai' => ChienDichPostback::HOAT_DONG,
            'note_status' => ChienDichPostback::APPROVED])
            ->all();

        return [
          'chienDich' => $chienDich
        ];
    }

    public function actionGetMaGioiThieu(){
        $user = User::find()->all();

        foreach ($user as $item){
            $ho_ten = str_replace('http://localhost/Vietlixi-App/', 'https://vietlixi.com/', $item->link);

            $item->updateAttributes(['link' => $ho_ten]);
        }

        return [
            'status' => 1
        ];
    }

    public function actionDoiMaGioiThieu(){
        $user = User::findOne([$_POST['user']['uid']]);

        if (is_null($user)){
            return [
                'status' => 'failed',
                'content' => 'Người dùng không tồn tại'
            ];
        }

        $ma_gioi_thieu = User::findAll(['ma_gioi_thieu'=>$_POST['maGioiThieu'], 'status'=>'10']);

        if (count($ma_gioi_thieu) > 0){
            return [
                'status' => 'failed',
                'content' => 'Mã giới thiệu đã tồn tại, vui lòng nhập mã giới thiệu khác'
            ];
        }

        $user->updateAttributes(['ma_gioi_thieu' => $_POST['maGioiThieu']]);
        $new_link = "https://vietlixi.com/app-register.html?ma_gioi_thieu=".$_POST['maGioiThieu'];
        $user->updateAttributes(['link' => $new_link]);
        return [
            'status' => 'success',
            'content' => 'Đổi mã giới thiệu thành công'
        ];
    }

    /** Lấy link fanpage facebook và sđt zalo */
    public function actionGetContact(){
        $facebook = DanhMuc::find()->select('link')->andFilterWhere(['active' => 1, 'type' => DanhMuc::FANPAGE])->one();
        $zalo = DanhMuc::find()->select('link')->andFilterWhere(['active' => 1, 'type' => DanhMuc::Zalo])->one();

        return [
            'facebook' => $facebook['link'],
            'zalo' => "https://zalo.me/".$zalo['link']
        ];
    }


//    public function actionSaveBankName(){
//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.vietqr.io/v2/banks',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'GET',
//            CURLOPT_HTTPHEADER => array(
//                'Cookie: connect.sid=s%3Aw65X-PXYIAEiALOLVvp-abAK1OY065cc.aJuCyMsOVU9xheFyiprziDuDv1qWAM9epWUz7V1VEFM'
//            ),
//        ));
//
//        $response = json_decode(curl_exec($curl));
//
//        curl_close($curl);
//
//        foreach ($response->data as $item){
//            $danhMuc = new DanhMuc();
//            $danhMuc->name = $item->shortName;
//            $danhMuc->type = DanhMuc::TEN_NGAN_HANG;
//            $danhMuc->active = 1;
//            $danhMuc->image = $item->logo;
//
//            $danhMuc->save();
//        }
//
//        return [
//            'status' => 1,
//        ];
//    }
}
