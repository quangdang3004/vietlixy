<?php
namespace backend\controllers;

use backend\models\Cauhinh;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Security;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\HttpException;
use \yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['error', 'login', 'update'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        return $this->redirect(Url::toRoute('user/index'));
    }

    /** login */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->renderPartial('login', [
                'model' => $model,
            ]);
        }
    }

    /** logout */
    public function actionLogout()
    {
        User::updateAll(['auth_key' => ''], ['id' => Yii::$app->user->id]);
        Yii::$app->user->logout();
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('token');
        $cookies->remove('userId');
        $cookies->remove('username');
        unset($cookies['token']);
        unset($cookies['userId']);
        unset($cookies['username']);
        return $this->goHome();
    }

    /** error */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if(Yii::$app->request->isAjax){
                echo myAPI::getMessage('danger', $exception->getMessage());
                exit;
            }
            else
                return $this->render('error', ['exception' => $exception]);
        }
    }

    /** doimatkhau */
    public function actionDoimatkhau(){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $sercurity = new Security();
        $user = User::findOne(Yii::$app->user->getId());
        $user->password_hash = $_POST['matkhaucu'];

        if(!Yii::$app->user->login($user))
            throw new HttpException(500, myAPI::getMessage('danger', 'Mật khẩu cũ không đúng!'));
        else{
            $matkhaumoi = $sercurity->generatePasswordHash(trim($_POST['matkhaumoi']));
            User::updateAll(['password_hash' => $matkhaumoi], ['id' => Yii::$app->user->getId()]);
            return ['message' => myAPI::getMessage('success', 'Đã đổi mật khẩu thành công')];
        }
    }

    /** update */
    public function actionUpdate(){
//        SoTietKiem::findOne(5)->delete();
//        $lichSuThanhToans = LichSuThanhToan::find()->all();
//        foreach ($lichSuThanhToans as $lichSuThanhToan) {
//            /** @var $lichSuThanhToan LichSuThanhToan */
//            if (!is_null($lichSuThanhToan->huy_dong_von_id)) {
//                $lichSuThanhToan->updateAttributes(['ma_khoan_vay' => $lichSuThanhToan->huyDongVon->ma_huy_dong_von]);
//            }
//            if (!is_null($lichSuThanhToan->vay_dao_han_id)) {
//                $lichSuThanhToan->updateAttributes(['ma_khoan_vay' => $lichSuThanhToan->vayDaoHan->ma_vay_dao_han]);
//            }
//            if (!is_null($lichSuThanhToan->vay_dai_han_id)) {
//                $lichSuThanhToan->updateAttributes(['ma_khoan_vay' => $lichSuThanhToan->vayDaiHan->ma_vay_dai_han]);
//            }
//            if (!is_null($lichSuThanhToan->vay_tra_gop_id)) {
//                $lichSuThanhToan->updateAttributes(['ma_khoan_vay' => $lichSuThanhToan->vayTraGop->ma_vay_tra_gop]);
//            }
//        }
    }
}
