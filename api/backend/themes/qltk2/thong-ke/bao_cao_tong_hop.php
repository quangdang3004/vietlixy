<?php
/** @var $goc_dao_han double */
/** @var $lai_dao_han double */
/** @var $goc_dai_han double */
/** @var $lai_dai_han double */
/** @var $goc_tra_gop double */
/** @var $tai_khoan double */
/** @var $tien_mat double */
/** @var $stk_gui double */
/** @var $stk_gui_lai double */
/** @var $stk_vay double */
/** @var $stk_vay_lai double */
/** @var $trich_lap double */
/** @var $tai_san double */
/** @var $tai_san_fix double */
/** @var $quy_vp double */
/** @var $phuc_loi double */
/** @var $huy_dong_dai_han double */
/** @var $huy_dong_dai_han_lai double */
/** @var $huy_dong_ngan_han double */
/** @var $huy_dong_ngan_han_lai double */
?>

<table class="table table-bordered">
    <tr>
        <td>Quỹ KD</td>
        <td class="text-right"><strong><?= number_format($goc_dao_han + $lai_dao_han + $goc_dai_han + $lai_dai_han +
                    $goc_tra_gop + $stk_gui + $stk_gui_lai - $stk_vay - $stk_vay_lai + $tai_san - $huy_dong_dai_han - $huy_dong_dai_han_lai -
                    $huy_dong_ngan_han - $huy_dong_ngan_han_lai - $trich_lap + $tai_khoan + $tien_mat, 0, '.', ',') ?></strong></td>
        <td colspan="3"></td>
        <td class="text-right"><strong><?= number_format($tai_khoan + $tien_mat, 0, '.', ',') ?></strong></td>
    </tr>
    <tr>
        <td>Cho vay</td>
        <td class="text-right"><?= number_format($goc_dao_han + $lai_dao_han + $goc_dai_han + $lai_dai_han + $goc_tra_gop + $tai_khoan + $tien_mat, 0, '.', ',') ?></td>
        <td class="text-right">ĐH</td>
        <td class="text-right">Dài hạn</td>
        <td class="text-right">TG</td>
        <td class="text-right">TK</td>
        <td class="text-right">TM</td>
    </tr>
    <tr>
        <td rowspan="3"></td>
        <td class="text-right">Gốc</td>
        <td class="text-right"><?= number_format($goc_dao_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($goc_dai_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($goc_tra_gop, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($tai_khoan, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($tien_mat, 0, '.', ',') ?></td>
    </tr>
    <tr>
        <td class="text-right">Lãi dự phòng</td>
        <td class="text-right"><?= number_format($lai_dao_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($lai_dai_han, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td class="text-right">Tổng</td>
        <td class="text-right"><?= number_format($goc_dao_han + $lai_dao_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($goc_dai_han + $lai_dai_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($goc_tra_gop, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($tai_khoan, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($tien_mat, 0, '.', ',') ?></td>
    </tr>
    <tr>
        <td>STK</td>
        <td class="text-right"><?= number_format($stk_gui + $stk_gui_lai - $stk_vay - $stk_vay_lai, 0, '.', ',') ?></td>
        <td class="text-right">Tiền gửi</td>
        <td class="text-right">Tiền vay CC</td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td rowspan="3"></td>
        <td class="text-right">Gốc</td>
        <td class="text-right"><?= number_format($stk_gui, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($stk_vay, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td class="text-right">Lãi dự phòng</td>
        <td class="text-right"><?= number_format($stk_gui_lai, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($stk_vay_lai, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td class="text-right">Tổng</td>
        <td class="text-right"><?= number_format($stk_gui + $stk_gui_lai, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($stk_vay + $stk_vay_lai, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td>Huy động</td>
        <td class="text-right"><?= number_format($huy_dong_dai_han + $huy_dong_dai_han_lai + $huy_dong_ngan_han + $huy_dong_ngan_han_lai, 0, '.', ',') ?></td>
        <td class="text-right">Dài hạn</td>
        <td class="text-right">Ngắn hạn</td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td rowspan="3"></td>
        <td class="text-right">Gốc</td>
        <td class="text-right"><?= number_format($huy_dong_dai_han, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($huy_dong_ngan_han, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td class="text-right">Lãi dự phòng</td>
        <td class="text-right"><?= number_format($huy_dong_dai_han_lai, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($huy_dong_ngan_han_lai, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td class="text-right">Tổng</td>
        <td class="text-right"><?= number_format($huy_dong_dai_han + $huy_dong_dai_han_lai, 0, '.', ',') ?></td>
        <td class="text-right"><?= number_format($huy_dong_ngan_han + $huy_dong_ngan_han_lai, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td>Trích lập</td>
        <td class="text-right"><?= number_format($trich_lap, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td>Tài sản</td>
        <td class="text-right"><?= number_format($tai_san_fix, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td>Quỹ VP</td>
        <td class="text-right"><?= number_format($quy_vp, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
    <tr>
        <td>Phúc lợi</td>
        <td class="text-right"><?= number_format($phuc_loi, 0, '.', ',') ?></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
    </tr>
</table>

