<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\grid\DataColumn;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\HouseBillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'QUẢN LÝ SẢN PHẨM');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="text-center text-primary">PHẦN MỀM QUẢN LÝ SẢN PHẨM - IPRODUCT</h1>

<!--<div class="row">-->
<!--    <div class="col-md-6">-->
<!--        <h4>Thông tin khai thác hàng trong tháng</h4>-->
<!--        <div id="chartdiv"></div>-->
<!--    </div>-->
<!--    <div class="col-md-6">-->
<!--        <h4>Số lượng cont(s) trong tháng</h4>-->
<!--        <div id="chartdiv-2"></div>-->
<!--    </div>-->
<!--</div>-->

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/themes/qltk2/assets/global/plugins/amcharts/amcharts/amcharts.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/themes/qltk2/assets/global/plugins/amcharts/amcharts/serial.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/themes/qltk2/assets/global/plugins/amcharts/amcharts/themes/light.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/index-site.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>