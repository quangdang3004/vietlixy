<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_qua_tang_thanh_vien}}".
 *
 * @property int $id
 * @property int|null $qua_tang_id
 * @property int|null $khach_hang_id
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $nguoi_duyet_id
 * @property string|null $ngay_duyet
 * @property int|null $active
 * @property int|null $so_luong
 * @property float|null $don_gia
 * @property float|null $tong_tien
 * @property string|null $name
 * @property string|null $hoten
 */
class QuanLyQuaTangThanhVien extends \yii\db\ActiveRecord
{
    const DA_DUYET = 'Đã duyệt';
    const CHO_DUYET = 'Chờ duyệt';
    const HUY = 'Hủy';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_qua_tang_thanh_vien}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'qua_tang_id', 'khach_hang_id', 'user_id', 'nguoi_duyet_id', 'active', 'so_luong'], 'integer'],
            [['trang_thai'], 'string'],
            [['created', 'ngay_duyet'], 'safe'],
            [['don_gia', 'tong_tien'], 'number'],
            [['name'], 'string', 'max' => 300],
            [['hoten'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qua_tang_id' => 'Qua Tang ID',
            'khach_hang_id' => 'Khach Hang ID',
            'trang_thai' => 'Trang Thai',
            'created' => 'Created',
            'user_id' => 'User ID',
            'nguoi_duyet_id' => 'Nguoi Duyet ID',
            'ngay_duyet' => 'Ngay Duyet',
            'active' => 'Active',
            'so_luong' => 'So Luong',
            'don_gia' => 'Don Gia',
            'tong_tien' => 'Tong Tien',
            'name' => 'Name',
            'hoten' => 'Hoten',
        ];
    }
}
