<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%chi_tiet_banner}}".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $link
 * @property string|null $region
 * @property float|null $vi_tri
 * @property int|null $active
 * @property string|null $created
 * @property int|null $user_id
 * @property string|null $updated
 * @property int|null $user_updated_id
 * @property string|null $image
 * @property int|null $image_banner_id
 */
class ChiTietBanner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%chi_tiet_banner}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'active', 'user_id', 'user_updated_id', 'image_banner_id'], 'integer'],
            [['region'], 'string'],
            [['vi_tri'], 'number'],
            [['created', 'updated'], 'safe'],
            [['title', 'link', 'image'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'region' => 'Region',
            'vi_tri' => 'Vi Tri',
            'active' => 'Active',
            'created' => 'Created',
            'user_id' => 'User ID',
            'updated' => 'Updated',
            'user_updated_id' => 'User Updated ID',
            'image' => 'Image',
            'image_banner_id' => 'Image Banner ID',
        ];
    }

  public function getBannersByType($type){
    return self::find()->andFilterWhere(['active' => 1, 'region' => $type])
      ->orderBy(['vi_tri' => SORT_ASC])
      ->all();
  }
}
