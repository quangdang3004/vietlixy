<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%lich_su_dong_bo_chien_dich}}".
 *
 * @property int $id
 * @property string|null $thoi_gian
 * @property string|null $duong_dan_dong_bo
 * @property int|null $da_dong_bo
 */
class LichSuDongBoChienDich extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lich_su_dong_bo_chien_dich}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['thoi_gian'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'thoi_gian' => 'Thoi Gian',
        ];
    }
}
