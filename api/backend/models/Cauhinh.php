<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $content
 * @property string $name
 * @property string $ghi_chu
 * @property string|null $ngay_xac_nhan
 */
class Cauhinh extends ActiveRecord
{
    const VI_DIEN_TU = 'vi_dien_tu';
    const TIEN_DAU_NGAY = 'tien_dau_ngay';

    public static function tableName()
    {
        return '{{%cauhinh}}';
    }

    public function rules()
    {
        return [
            [['ghi_chu', 'name'], 'safe'],
            [['content', 'ngay_xac_nhan'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Nội dung',
            'ghi_chu' => 'Ký hiệu',
            'name' => 'Tên',
            'ngay_xac_nhan' => 'Ngày xác nhận',
        ];
    }
}
