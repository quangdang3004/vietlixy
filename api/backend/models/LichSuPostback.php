<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%lich_su_postback}}".
 *
 * @property int $id
 * @property string|null $status
 * @property string|null $reward
 * @property string|null $note_status
 * @property string|null $created
 * @property int|null $postback_id
 *
 * @property Postback $postback
 */
class LichSuPostback extends \yii\db\ActiveRecord
{
    const REJECTED = 2;
    const APPROVED = 1;
    const Pending = 0;
    const REJECTED_STR = 'Rejected';
    const Pending_STR = 'Pending';
    const APPROVED_STR = 'Approved';
    public static $statusArr = [
        self::REJECTED => self::REJECTED_STR,
        self::APPROVED => self::APPROVED_STR,
        self::Pending => self::Pending_STR,
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lich_su_postback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reward'], 'string'],
            [['created', 'note_status', 'status'], 'safe'],
            [['postback_id'], 'integer'],
            [['postback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Postback::className(), 'targetAttribute' => ['postback_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'reward' => 'Reward',
            'created' => 'Created',
            'postback_id' => 'Postback ID',
        ];
    }

    /**
     * Gets query for [[Postback]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPostback()
    {
        return $this->hasOne(Postback::className(), ['id' => 'postback_id']);
    }

    public function beforeSave($insert)
    {
        $this->note_status = self::$statusArr[intval($this->status)];
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        $model = Postback::findOne($this->postback_id);
        $model->updateAttributes([
            'reward' => $this->reward,
            'status' => $this->status,
            'note_status' => $this->note_status
        ]);
        // Nếu Approved thì tạo 1 giao dịch cộng tiền vào ví của người chơi
        if($this->note_status == self::APPROVED_STR){
            $modelGD = new LichSuGiaoDich();
            $modelGD->chien_dich_post_back_id = $this->postback_id;
            $modelGD->cost = $this->reward;
            $modelGD->type = LichSuGiaoDich::HOA_HONG;
            $modelGD->created = date("Y-m-d H:i:s");
            $modelGD->trang_thai = LichSuGiaoDich::DA_DUYET;
            $modelGD->user_id = $model->user_id;
            $modelGD->save();

            $khachHang = User::findOne($model->user_id);
            $khachHang->updateAttributes([
                'vi_dien_tu' => $khachHang->vi_dien_tu + $this->reward
            ]);
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
