<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\HttpException;

/**
 * @property int $id
 * @property string|null $ma_chien_dich
 * @property string|null $title
 * @property int|null $network_id
 * @property int|null $category_id
 * @property int|null $type_id
 * @property int|null $device_id
 * @property string|null $track_link
 * @property string|null $preview_offer
 * @property string|null $image
 * @property float|null $hoa_hong_tu_net
 * @property string|null $type_hoa_hong
 * @property float|null $Capped
 * @property float|null $chi_phi_chien_dich
 * @property int|null $Request
 * @property int|null $active
 * @property string|null $conversion_requirements
 * @property string|null $created
 * @property string|null $trang_thai
 * @property string|null $link_video
 * @property int|null $user_id
 * @property int|null $noi_bat
 * @property int|null $danh_muc_id
 * @property float|null $vi_tri_hien_thi
 * @property float|null $utm_source
 *
 * @property DanhMuc $category
 * @property DanhMuc $device
 * @property DanhMuc $type
 * @property Network $network
 * @property QuanLyChienDich $user_name
 * @property User $user
 * @property ThucHienChienDich[] $thucHienChienDiches
 * @property TrangThaiChienDich[] $trangThaiChienDiches
 */
class ChienDich extends ActiveRecord
{
    const KHOI_TAO = 'Khởi tạo';
    const HOAT_DONG = 'Hoạt động';
    const KHONG_HOAT_DONG = 'Không hoạt động';
    const CHO_DUYET = 'Chờ duyệt';

    const NOI_BAT = 'Nổi bật';
    const KHONG_NOI_BAT = 'Không nổi bật';

    public static function tableName()
    {
        return '{{%chien_dich}}';
    }

    public function rules()
    {
        return [
            [['network_id', 'category_id', 'type_id', 'device_id', 'Request', 'active', 'user_id', 'noi_bat', 'utm_source'], 'integer'],
            [['hoa_hong_tu_net', 'Capped', 'vi_tri_hien_thi'], 'number'],
            [['conversion_requirements', 'preview_offer'], 'string'],
            [['created', 'danh_muc_id', 'trang_thai', 'chi_phi_chien_dich'], 'safe'],
            [['ma_chien_dich'], 'string', 'max' => 30],
            [['title', 'track_link', 'image'], 'string', 'max' => 300],
            [['type_hoa_hong', 'link_video'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['device_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['network_id'], 'exist', 'skipOnError' => true, 'targetClass' => Network::className(), 'targetAttribute' => ['network_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ma_chien_dich' => 'Ma Chien Dich',
            'title' => 'Title',
            'network_id' => 'Network ID',
            'category_id' => 'Category ID',
            'type_id' => 'Type ID',
            'device_id' => 'Device ID',
            'track_link' => 'Track Link',
            'preview_offer' => 'Preview Offer',
            'image' => 'Image',
            'hoa_hong_tu_net' => 'Hoa Hong Tu Net',
            'type_hoa_hong' => 'Type Hoa Hong',
            'Capped' => 'Capped',
            'Request' => 'Request',
            'active' => 'Active',
            'conversion_requirements' => 'Conversion Requirements',
            'created' => 'Created',
            'user_id' => 'User ID',
            'noi_bat' => 'Noi Bat',
            'vi_tri_hien_thi' => 'Vi Tri Hien Thi',
            'utm_source' => 'Utm Source',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'category_id']);
    }

    public function getDevice()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'device_id']);
    }

    public function getType()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'type_id']);
    }

    public function getNetwork()
    {
        return $this->hasOne(Network::className(), ['id' => 'network_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getThucHienChienDiches()
    {
        return $this->hasMany(ThucHienChienDich::className(), ['chien_dich_id' => 'id']);
    }

    public function getTrangThaiChienDiches()
    {
        return $this->hasMany(TrangThaiChienDich::className(), ['chien_dich_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->active = 1;
            $this->created = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        $trangThaiChienDich = new TrangThaiChienDich();
        $trangThaiChienDich->trang_thai = $this->trang_thai;
        $trangThaiChienDich->chien_dich_id = $this->id;
        $trangThaiChienDich->created = date('Y-m-d H:i:s');
        $trangThaiChienDich->user_id = $this->user_id;
        if (!$trangThaiChienDich->save()) {
            throw new HttpException(500, Html::errorSummary($trangThaiChienDich));
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
