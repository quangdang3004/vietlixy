<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%qua_tang}}".
 *
 * @property int $id
 * @property int $active
 * @property int $user_updated_id
 * @property string|null $name
 * @property string|null $updated
 * @property string|null $anh_dai_dien
 * @property float|null $gia_tri
 * @property string|null $created
 * @property int|null $user_id
 *
 * @property QuaTangThanhVien[] $quaTangThanhViens
 */
class QuaTang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%qua_tang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gia_tri'], 'number'],
            [['created', 'active', 'user_updated_id','updated'], 'safe'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 300],
            [['anh_dai_dien'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'anh_dai_dien' => 'Anh Dai Dien',
            'gia_tri' => 'Gia Tri',
            'created' => 'Created',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[QuaTangThanhViens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuaTangThanhViens()
    {
        return $this->hasMany(QuaTangThanhVien::className(), ['qua_tang_id' => 'id']);
    }
}
