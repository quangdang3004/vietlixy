<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_user}}".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $anhdaidien
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property int|null $khu_vuc_id
 * @property string|null $role_user
 * @property string|null $CIF
 * @property string|null $ngay_cap
 * @property string|null $noi_cap
 * @property int|null $nguoi_gioi_thieu_id
 * @property int|null $nguoI_tham_chieu_id
 * @property string|null $dai_ly
 * @property string|null $khu_vuc
 * @property string|null $nguoi_gioi_thieu
 * @property string|null $nguoi_tham_chieu
 */
class QuanLyUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'dai_ly_id', 'khu_vuc_id', 'nguoi_gioi_thieu_id', 'nguoi_tham_chieu_id'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten', 'CIF', 'ngay_cap', 'noi_cap', 'dai_ly', 'khu_vuc', 'nguoi_gioi_thieu', 'nguoi_tham_chieu'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
            [['role_user'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'dia_chi' => 'Dia Chi',
            'cmnd' => 'Cmnd',
            'anhdaidien' => 'Anhdaidien',
            'dai_ly_id' => 'Dai Ly ID',
            'ngay_sinh' => 'Ngay Sinh',
            'code_ho_ten' => 'Code Ho Ten',
            'khu_vuc_id' => 'Khu Vuc ID',
            'role_user' => 'Role User',
            'CIF' => 'Cif',
            'ngay_cap' => 'Ngay Cap',
            'noi_cap' => 'Noi Cap',
            'nguoi_gioi_thieu_id' => 'Nguoi Gioi Thieu ID',
            'nguoI_tham_chieu_id' => 'Nguo I Tham Chieu ID',
            'dai_ly' => 'Dai Ly',
            'khu_vuc' => 'Khu Vuc',
            'nguoi_gioi_thieu' => 'Nguoi Gioi Thieu',
            'nguoi_tham_chieu' => 'Nguoi Tham Chieu',
        ];
    }
}
