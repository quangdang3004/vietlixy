<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string|null $Name
 * @property string|null $Subid
 * @property string|null $Order
 * @property string|null $Postback_Affsub_id
 * @property int|null $active
 * @property string|null $Postback_pass
 * @property string|null $Rate
 * @property string|null $Postback_Credit
 * @property string|null $Postback_Offer
 */
class Network extends ActiveRecord
{
    public static function tableName()
    {
        return 'tc_network';
    }

    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['Name', 'Subid', 'Order', 'Postback_Affsub_id', 'Postback_pass', 'Rate', 'Postback_Credit', 'Postback_Offer'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Subid' => 'Subid',
            'Order' => 'Order',
            'Postback_Affsub_id' => 'Postback Affsub ID',
            'active' => 'Active',
            'Postback_pass' => 'Postback Pass',
            'Rate' => 'Rate',
            'Postback_Credit' => 'Postback Credit',
            'Postback_Offer' => 'Postback Offer',
        ];
    }

    public function getNetwork(){
        $data = self::find()
            ->groupBy(['Name'])
            ->all();

        $arr_data = [];
        /** @var Network $item */
        foreach ($data as $item) {
            $arr_data[] = ['label' => $item->Name, 'key' => $item->id];
        }
        return $arr_data;
    }
}
