<?php

namespace backend\models;

use Yii;
use yii\helpers\Html;
use common\models\User;
use yii\web\HttpException;

/**
 * @property int $id
 * @property int|null $chien_dich_post_back_id
 * @property float|null $cost
 * @property string|null $type // 'Hoa hồng', 'Rút tiền'
 * @property string|null $created
 * @property string|null $updated
 * @property string|null $trang_thai
 * @property string|null $ghi_chu
 * @property int|null $active
 * @property string|null $username
 * @property string|null $bank_name
 * @property int|null $bank_number
 * @property int|null $user_id
 * @property int|null $nguoi_duyet_id
 *
 * @property Postback $chienDichPostBack
 * @property User $user
 * @property User $nguoiDuyet
 */
class LichSuGiaoDich extends \yii\db\ActiveRecord
{
    const RUT_TIEN = 'Rút tiền';
    const HOA_HONG = 'Hoa hồng';
    const DOI_QUA = 'Đổi quà';

    const CHO_DUYET = 'Chờ duyệt';
    const DA_DUYET = 'Đã duyệt';
    const DA_HUY = 'Đã hủy';

    public static function tableName()
    {
        return '{{%lich_su_giao_dich}}';
    }

    public function rules()
    {
        return [
            [['chien_dich_post_back_id', 'cost', 'active', 'user_id'], 'integer'],
            [['cost'], 'number', 'min' => 0],
            [['username'], 'string', 'max' => 100],
            [['type', 'trang_thai', 'ghi_chu'], 'string'],
            [['created', 'updated'], 'safe'],
            [['chien_dich_post_back_id'], 'exist', 'skipOnError' => true, 'targetClass' => Postback::className(), 'targetAttribute' => ['chien_dich_post_back_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['nguoi_duyet_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['nguoi_duyet_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chien_dich_post_back_id' => 'Chien Dich Post Back ID',
            'cost' => 'Số tiền',
            'type' => 'Type',
            'created' => 'Created',
            'updated' => 'Updated',
            'trang_thai' => 'Trang Thai',
            'ghi_chu' => 'Ghi Chu',
            'active' => 'Active',
            'username' => 'Username',
            'user_id' => 'User ID',
            'nguoi_duyet_id' => 'Nguoi Duyet ID',
        ];
    }

    public function getChienDichPostBack()
    {
        return $this->hasOne(Postback::className(), ['id' => 'chien_dich_post_back_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getNguoiDuyet()
    {
        return $this->hasOne(User::className(), ['id' => 'nguoi_duyet_id']);
    }

    public function beforeSave($insert)
    {
        $this->username = $this->user->username;
        if ($insert) {
            $this->created = date('Y-m-d H:i:s');
            $this->trang_thai = self::CHO_DUYET;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        $trangThaiChienDich = new TrangThaiGiaoDich();
        $trangThaiChienDich->trang_thai = $this->trang_thai;
        $trangThaiChienDich->giao_dich_id = $this->id;
        $trangThaiChienDich->created = date('Y-m-d H:i:s');
        $trangThaiChienDich->user_id = $this->user_id;
        if (!$trangThaiChienDich->save()) {
            throw new HttpException(500, Html::errorSummary($trangThaiChienDich));
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
