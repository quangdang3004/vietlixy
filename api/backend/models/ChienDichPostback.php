<?php

namespace backend\models;

use Yii;

/**
 * @property int $id
 * @property string $ma_chien_dich
 * @property string|null $title
 * @property int|null $network_id
 * @property int|null $category_id
 * @property int|null $type_id
 * @property int|null $device_id
 * @property string|null $track_link
 * @property string|null $preview_offer
 * @property string|null $image
 * @property float|null $hoa_hong_tu_net
 * @property string|null $type_hoa_hong
 * @property float|null $Capped
 * @property int|null $Request
 * @property int|null $active
 * @property string|null $conversion_requirements
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $noi_bat
 * @property float|null $vi_tri_hien_thi
 * @property int|null $danh_muc_id
 * @property string|null $trang_thai
 * @property float|null $chi_phi_chien_dich
 * @property int|null $utm_source
 * @property string $ngay_thuc_hien
 * @property string|null $trang_thai_postback
 * @property float|null $hoa_hong
 * @property float|null $so_tien_nhan_duoc
 * @property int|null $nguoi_thuc_hien_id
 * @property string|null $nguoi_thuc_hien
 */
class ChienDichPostback extends \yii\db\ActiveRecord
{
    const HOAT_DONG = 'Hoạt động';
    const APPROVED = 'Approved';

    public static function tableName()
    {
        return '{{%chien_dich_postback}}';
    }

    public function rules()
    {
        return [
            [['id', 'network_id', 'category_id', 'type_id', 'device_id', 'Request', 'active', 'user_id', 'noi_bat', 'danh_muc_id', 
            'utm_source', 'nguoi_thuc_hien_id'], 'integer'],
            [['track_link', 'type_hoa_hong', 'conversion_requirements', 'preview_offer', 'trang_thai', 'trang_thai_postback'], 'string'],
            [['hoa_hong_tu_net', 'Capped', 'vi_tri_hien_thi', 'chi_phi_chien_dich', 'hoa_hong', 'so_tien_nhan_duoc'], 'number'],
            [['created', 'ngay_thuc_hien'], 'safe'],
            [['ma_chien_dich'], 'string', 'max' => 30],
            [['title', 'image'], 'string', 'max' => 200],
            [['nguoi_thuc_hien'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ma_chien_dich' => 'Ma Chien Dich',
            'title' => 'Title',
            'network_id' => 'Network ID',
            'category_id' => 'Category ID',
            'type_id' => 'Type ID',
            'device_id' => 'Device ID',
            'track_link' => 'Track Link',
            'preview_offer' => 'Preview Offer',
            'image' => 'Image',
            'hoa_hong_tu_net' => 'Hoa Hong Tu Net',
            'type_hoa_hong' => 'Type Hoa Hong',
            'Capped' => 'Capped',
            'Request' => 'Request',
            'active' => 'Active',
            'conversion_requirements' => 'Conversion Requirements',
            'created' => 'Created',
            'user_id' => 'User ID',
            'noi_bat' => 'Noi Bat',
            'vi_tri_hien_thi' => 'Vi Tri Hien Thi',
            'danh_muc_id' => 'Danh Muc ID',
            'trang_thai' => 'Trang Thai',
            'chi_phi_chien_dich' => 'Chi Phi Chien Dich',
            'utm_source' => 'Utm Source',
            'ngay_thuc_hien' => 'Ngay Thuc Hien',
            'trang_thai_postback' => 'Trang Thai Postback',
            'hoa_hong' => 'Hoa Hong',
            'so_tien_nhan_duoc' => 'So Tien Nhan Duoc',
            'nguoi_thuc_hien_id' => 'Nguoi Thuc Hien ID',
            'nguoi_thuc_hien' => 'Nguoi Thuc Hien',
        ];
    }
}
