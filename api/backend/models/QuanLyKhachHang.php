<?php

namespace backend\models;

use Yii;

/**
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $anhdaidien
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property int|null $khu_vuc_id
 * @property string|null $role_user
 * @property string|null $CIF
 * @property string|null $ngay_cap
 * @property int|null $nguoi_gioi_thieu_id
 * @property int|null $nguoI_tham_chieu_id
 * @property string|null $noi_cap
 * @property string|null $name_mate
 * @property string|null $phone_mate
 * @property string|null $type_customer
 * @property string|null $household_address
 * @property string|null $dai_ly
 * @property string|null $khu_vuc
 * @property string|null $nguoi_gioi_thieu
 * @property string|null $nguoi_tham_chieu
 */
class QuanLyKhachHang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%quan_ly_khach_hang}}';
    }

    public function rules()
    {
        return [
            [['id', 'status', 'dai_ly_id', 'khu_vuc_id', 'nguoi_gioi_thieu_id', 'nguoI_tham_chieu_id'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh'], 'safe'],
            [['type_customer'], 'string'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten', 'CIF', 'ngay_cap', 'noi_cap', 'name_mate', 'household_address', 'dai_ly', 'khu_vuc', 'nguoi_gioi_thieu', 'nguoi_tham_chieu'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
            [['role_user', 'phone_mate'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
            'dia_chi' => 'Dia Chi',
            'cmnd' => 'Cmnd',
            'anhdaidien' => 'Anhdaidien',
            'dai_ly_id' => 'Dai Ly ID',
            'ngay_sinh' => 'Ngay Sinh',
            'code_ho_ten' => 'Code Ho Ten',
            'khu_vuc_id' => 'Khu Vuc ID',
            'role_user' => 'Role User',
            'CIF' => 'Cif',
            'ngay_cap' => 'Ngay Cap',
            'nguoi_gioi_thieu_id' => 'Nguoi Gioi Thieu ID',
            'nguoI_tham_chieu_id' => 'Nguo I Tham Chieu ID',
            'noi_cap' => 'Noi Cap',
            'name_mate' => 'Name Mate',
            'phone_mate' => 'Phone Mate',
            'type_customer' => 'Type Customer',
            'household_address' => 'Household Address',
            'dai_ly' => 'Dai Ly',
            'khu_vuc' => 'Khu Vuc',
            'nguoi_gioi_thieu' => 'Nguoi Gioi Thieu',
            'nguoi_tham_chieu' => 'Nguoi Tham Chieu',
        ];
    }
}
