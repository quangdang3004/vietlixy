<?php
return [
    'vendorPath' => dirname(dirname(dirname(dirname(__DIR__)))) . '/new-hrm/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
