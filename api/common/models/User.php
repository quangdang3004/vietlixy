<?php
namespace common\models;

use backend\models\DanhMuc;
use backend\models\UserVaiTro;
use backend\models\VaiTro;
use backend\models\Vaitrouser;
use Yii;
use yii\base\ErrorException;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\helpers\Html;
use yii\web\IdentityInterface;
use yii\web\HttpException;

/**
 * @property int $id
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $dia_chi
 * @property string|null $cmnd
 * @property string|null $anhdaidien
 * @property int|null $dai_ly_id
 * @property string|null $ngay_sinh
 * @property string|null $code_ho_ten
 * @property int|null $khu_vuc_id
 * @property string|null $ghi_chu
 * @property string|null $link
 * @property string|null $trang_thai
 * @property double|null $total
 * @property double|null $vi_dien_tu
 * @property double|null $current_total
 * @property double|null $pending_total
 * @property string|null $ma_gioi_thieu
 * @property int|null $so_lan_truy_cap
 *
 * @property User $daiLy
 * @property DanhMuc $khuVuc
 * @property User[] $users
 * @property User[] $users0
 * @property User[] $users1
 * @property Vaitrouser[] $vaitrousers
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $files;
    public $khach_hang = false;
    public $thanh_vien = 0;
    public $auth;
    public $uid;
    public $vai_tro;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function rules()
    {
        return [
            [['status', 'dai_ly_id', 'khu_vuc_id'], 'integer'],
            [['created_at', 'updated_at', 'ngay_sinh', 'total', 'link', 'so_lan_truy_cap'], 'safe'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'anhdaidien', 'code_ho_ten'], 'string', 'max' => 100],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'string', 'max' => 20],
            [['dia_chi'], 'string', 'max' => 200],
            [['username'], 'unique'],
            [['ma_gioi_thieu'], 'unique', 'message'=>'Mã giới thiệu đã tồn tại'],
            [['ghi_chu', 'trang_thai'], 'string'],
            [['current_total', 'pending_total', 'vi_dien_tu'], 'safe'],
            [['khu_vuc_id'], 'exist', 'skipOnError' => true, 'targetClass' => DanhMuc::className(), 'targetAttribute' => ['khu_vuc_id' => 'id']],
            [['dai_ly_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['dai_ly_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Tài khoản',
            'password_hash' => 'Mật khẩu',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'status' => 'Trạng thái',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'hoten' => 'Họ tên',
            'dien_thoai' => 'Điện thoại',
            'dia_chi' => 'Địa chỉ',
            'cmnd' => 'Cmnd',
            'anhdaidien' => 'Anhdaidiem',
            'dai_ly_id' => 'Dai Ly Id',
            'ngay_sinh' => 'Ngay Sinh',
            'code_ho_ten' => 'Code Ho Ten',
            'khu_vuc_id' => 'Khu Vuc Id',
            'ghi_chu' => 'Ghi Chu',
            'trang_thai' => 'Trang Thai',
            'total' => 'Total',
            'current_total' => 'Current Total',
            'pending_total' => 'Pending Total',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getDaiLy()
    {
        return $this->hasOne(User::className(), ['id' => 'dai_ly_id']);
    }

    public function getKhuVuc()
    {
        return $this->hasOne(DanhMuc::className(), ['id' => 'khu_vuc_id']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['dai_ly_id' => 'id']);
    }

    public function getUsers0()
    {
        return $this->hasMany(User::className(), ['nguoI_tham_chieu_id' => 'id']);
    }

    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }

    public function isAccess($arrRoles){
        return !is_null(Vaitrouser::find()->andFilterWhere(['in', 'vaitro', $arrRoles])->andFilterWhere(['user_id' => Yii::$app->user->getId()])->one());
    }

    public static function isViewAll($uid){
        return $uid == 1 || !is_null(UserVaiTro::findOne(['vai_tro' => VaiTro::QUAN_LY, 'id' => $uid]));
    }

    public function getVaitros($user){
        $arr = [];
        foreach ($user->vaitrousers as $vaitrouser) {
            $arr[] = $vaitrouser->vaitro;
        }
        return implode(', ',$arr);
    }

    public function beforeDelete()
    {
        Vaitrouser::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        $this->hoten = trim($this->hoten);
        $this->dien_thoai = trim($this->dien_thoai);
        $this->code_ho_ten = myAPI::createCode($this->hoten);

        if($insert){
            $this->created_at = date("Y-m-d H:i:s");
        }
        else{
            $this->updated_at = date("Y-m-d H:i:s");
        }

        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
      if($insert){
        if ($this->id != 1) {
          Vaitrouser::deleteAll(['user_id' => $this->id]);
          if (!is_null($this->vai_tro)){
              foreach ($this->vai_tro as $vai_tro_id) {
                  $vaitrouser = new Vaitrouser();
                  $vaitrouser->vaitro_id = $vai_tro_id;
                  $vaitrouser->user_id = $this->id;
                  if (!$vaitrouser->save()) {
                      throw new HttpException(500, Html::errorSummary($vaitrouser));
                  }
              }
          }


        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
      }

    }
}
