<?php


namespace common\models;


use backend\models\Cauhinh;
use backend\models\HuyDongVon;
use backend\models\HuyDongVonDuTinh;
use yii\helpers\VarDumper;

class TaiChinhAPI
{
    /**
     * @param $model HuyDongVon
     */
    public static function tinhLaiVay($model){
        $arr_bang_tra_lai = [];
        $soTienDuNo = $model->so_tien;
        $ngay = $model->ngay_bat_dau_tinh_lai;
        $soTienTraHangThangBanDau = $model->so_tien_tra_hang_thang;
        $soTienTraHangThang = $model->so_tien_tra_hang_thang;
        $ngayTrongThang = date("d", strtotime($ngay));
        for($i = 1; $i <= $model->ky_han; $i++){

            if($soTienDuNo < $soTienTraHangThangBanDau){
                $tienGocTraHangThang = $soTienDuNo;
                $soTienTraHangThang = round($soTienDuNo * $model->lai_suat/100 + $soTienDuNo);
            }else{
                $tienGocTraHangThang = round($soTienTraHangThang - round(($soTienDuNo * $model->lai_suat/100)));
            }

            $arr_bang_tra_lai[] = [
                'ngay' => $ngay,
                'du_no' => $soTienDuNo,
                'lai_thang' => round($soTienDuNo * $model->lai_suat / 100),
                'goc_tra_hang_thang' => $tienGocTraHangThang,
                'so_tien_tra_hang_thang' => $soTienTraHangThang,
            ];
            $thangTiepTheo = date("Y-m-d", strtotime('+1 months', strtotime($ngay)));
            if(date("m", strtotime($thangTiepTheo)) - date("m", strtotime($ngay)) == 2){
                $thang = date("m", strtotime($thangTiepTheo)) - 1;
                $nam = date("Y", strtotime($thangTiepTheo));
                $ngay = date("Y-m-t", strtotime("{$nam}-{$thang}-01"));
            }else{
                $ngayCuaThangTiepTheo = date("d", strtotime($thangTiepTheo));
                if($ngayCuaThangTiepTheo <= $ngayTrongThang && $ngayCuaThangTiepTheo <= date("t", strtotime($thangTiepTheo)))
                    $ngay = date("Y-m", strtotime($thangTiepTheo)).'-'.$ngayTrongThang;
                else
                    $ngay = $thangTiepTheo;
            }
            $soTienDuNo = $soTienDuNo - $tienGocTraHangThang;
        }

        return $arr_bang_tra_lai;
    }
}
