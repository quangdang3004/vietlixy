import Vue from 'vue'
import VueRouter from 'vue-router'

// Routes
import { canNavigate } from '@/libs/acl/routeProtection'
import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'
import apps from './routes/apps'
import dashboard from './routes/dashboard'
import uiElements from './routes/ui-elements/index'
import pages from './routes/pages'
import chartsMaps from './routes/charts-maps'
import formsTable from './routes/forms-tables'
import others from './routes/others'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    { path: '/', redirect: { name: 'dashboard-ecommerce' } },
    {
      path: '/danh-muc',
      name: 'danh-muc',
      component: () => import('@/views/danh-muc/index.vue'),
    },
    {
      path: '/khach-hang',
      name: 'khach-hang',
      component: () => import('@/views/users/khach-hang/index.vue'),
    },
    {
      path: '/giao-dich',
      name: 'giao-dich',
      component: () => import('@/views/giao-dich/index.vue'),
    },
    {
      path: '/thong-ke',
      name: 'thong-ke',
      component: () => import('@/views/thong-ke/index.vue'),
    },
    {
      path: '/dai-ly',
      name: 'dai-ly',
      component: () => import('@/views/users/dai-ly/index.vue'),
    },

    {
      path: '/thanh-vien',
      name: 'thanh-vien',
      component: () => import('@/views/users/thanh-vien/index.vue'),
    },
    {
      path: '/vai-tro',
      name: 'vai-tro',
      component: () => import('@/views/vai-tro/index.vue'),
    },
    {
      path: '/chuc-nang',
      name: 'chuc-nang',
      component: () => import('@/views/chuc-nang/index.vue'),
    },
    {
      path: '/phan-quyen',
      name: 'phan-quyen',
      component: () => import('@/views/phan-quyen/index.vue'),
    },
    {
      path: '/cau-hinh',
      name: 'cau-hinh',
      component: () => import('@/views/cau-hinh/index.vue'),
    },
    {
      path: '/banner',
      name: 'banner',
      component: () => import('@/views/banner/index.vue'),
    },
    {
      path: '/danh-muc-qua-tang',
      name: 'danh-muc-qua-tang',
      component: () => import('@/views/qua-tang/index.vue'),
    },
    {
      path: '/affiliate',
      name: 'affiliate',
      component: () => import('@/views/users/khach-hang/index.vue'),
    },
    {
      path: '/kiem-tien',
      name: 'kiem-tien',
      component: () => import('@/views/kiem-tien/index.vue'),
    },
    {
      path: '/kiem-tien-postback',
      name: 'kiem-tien-postback',
      component: () => import('@/views/kiem-tien/postback/index.vue'),
    },
    {
      path: '/profile',
      name: 'profile',
      // eslint-disable-next-line import/extensions
      component: () => import('@/views/users/profile/form'),
    },

    {
      path: '/xem-chi-tiet-postback/:utm_source',
      name: 'xem-chi-tiet-postback',
      component: () => import('@/views/kiem-tien/postback/dai-ly/view/index.vue'),
    },
    {
      path: '/them-chien-dich',
      name: 'them-chien-dich',
      component: () => import('@/views/kiem-tien/view/index.vue'),
    },
    {
      path: '/rut-tien',
      name: 'rut-tien',
      component: () => import('@/views/giao-dich/rut-tien/index.vue'),
    },
    {
      path: '/yeu-cau-rut-tien',
      name: 'yeu-cau-rut-tien',
      component: () => import('@/views/giao-dich/yeu-cau-rut-tien/index.vue'),
    },
    {
      path: '/yeu-cau-doi-qua',
      name: 'yeu-cau-doi-qua',
      component: () => import('@/views/giao-dich/yeu-cau-doi-qua/index.vue'),
    },
    {
      path: '/network',
      name: 'network',
      component: () => import('@/views/network/index.vue'),
    },
    ...apps,
    ...dashboard,
    ...pages,
    ...chartsMaps,
    ...formsTable,
    ...uiElements,
    ...others,
    {
      path: '*',
      redirect: 'error-404',
    },
  ],
})
const DEFAULT_TITLE = 'VietLixi'
router.afterEach(to => {
  // Use next tick to handle router history correctly
  // see: https://github.com/vuejs/vue-router/issues/914#issuecomment-384477609
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE
  })
})
router.beforeEach((to, _, next) => {
  const isLoggedIn = isUserLoggedIn()

  if (!canNavigate(to)) {
    // Redirect to login if not logged in
    if (!isLoggedIn) return next({ name: 'auth-login' })

    // If logged in => not authorized
    return next({ name: 'misc-not-authorized' })
  }

  // Redirect if logged in
  if (to.meta.redirectIfLoggedIn && isLoggedIn) {
    const userData = getUserData()
    next(getHomeRouteForLoggedInUser(userData ? userData.role_user : null))
  }

  return next()
})

export default router
