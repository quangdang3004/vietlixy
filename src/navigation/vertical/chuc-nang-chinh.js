export default [
  {
    title: 'Tổng quan ',
    route: 'dashboard-ecommerce',
    icon: 'LayersIcon',
  },
  {
    header: 'Danh mục',
  },
  {
    title: 'Khu vực',
    route: 'khu-vuc',
    icon: 'MapPinIcon',
  },
  {
    title: 'Các con số',
    route: 'y-nghia-cac-con-so',
    icon: 'FileTextIcon',
  },
  {
    header: 'Chức năng chính',
  },
  {
    title: 'Khách hàng',
    route: 'khach-hang',
    icon: 'UsersIcon',
  },
  {
    title: 'Đại lý',
    route: 'dai-ly',
    icon: 'GitMergeIcon',
  },
  {
    title: 'Giao dịch',
    route: 'giao-dich',
    icon: 'RepeatIcon',
  },
  {
    title: 'Thống kê',
    route: 'thong-ke',
    icon: 'PieChartIcon',
  },
  {
    header: 'Hệ thống',
  },
  {
    title: 'Network',
    route: 'network',
    icon: 'CloudIcon',
  },
  {
    title: 'Thành viên',
    route: 'thanh-vien',
    icon: 'UsersIcon',
  },
  {
    title: 'Vai trò',
    route: 'vai-tro',
    icon: 'AwardIcon',
  },
  {
    title: 'Chức năng',
    route: 'chuc-nang',
    icon: 'ToolIcon',
  },
  {
    title: 'Phân quyền',
    route: 'phan-quyen',
    icon: 'KeyIcon',
  },
  {
    title: 'Cấu hình',
    route: 'cau-hinh',
    icon: 'SettingsIcon',
  },
]
