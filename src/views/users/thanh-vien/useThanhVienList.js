import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import axios from 'axios'
// Notification
import router from '@/router'
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useThanhVienList() {
  // Use toast
  const toast = useToast()
  const refUserListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', label: 'STT', thStyle: { width: '1%' } },
    { key: 'username', sortable: true, label: 'Tên đăng nhập' },
    { key: 'hoten', sortable: true, label: 'Họ tên' },
    { key: 'dien_thoai', sortable: true, label: 'Điện thoại' },
    { key: 'vai_tro', sortable: true, label: 'Vai trò' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]

  const tableDonHangDaDatColumns = [
    { key: 'index', label: 'STT', thStyle: { width: '1%' } },
    { key: 'order_id', sortable: true, label: 'Mã vận đơn' },
    { key: 'trang_thai', sortable: true, label: 'Trạng thái' },
    { key: 'total', sortable: true, label: 'Tổng tiền' },
    { key: 'created', sortable: true, label: 'Ngày tạo' },
  ]

  const tableGiaoDichColumns = [
    { key: 'index', label: 'STT', thStyle: { width: '1%' } },
    { key: 'giao_dich_id', sortable: true, label: 'Mã Giao Dịch' },
    { key: 'type', sortable: true, label: 'Loại Giao Dịch' },
    { key: 'trang_thai', sortable: true, label: 'Trạng Thái' },
    { key: 'cost', sortable: true, label: 'Tổng Tiền' },
    { key: 'created', sortable: true, label: 'Ngày tạo' },
  ]

  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const fieldsTimKiem = ref([])

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }

  const currentUser = getUserData()

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })

  const getDonHang = (ctx, callback) => {
    store.state.showBlock = true

    store
      .dispatch('app-user/loadProfile', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        id: router.currentRoute.query.id_khach_hang,
      })
      .then(response => {
        const { donhang } = response.data
        callback(donhang)
        store.state.showBlock = false
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching Data list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  const getGiaoDich = (ctx, callback) => {
    store.state.showBlock = true

    store
      .dispatch('app-user/loadProfile', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        id: router.currentRoute.query.id_khach_hang,
      })
      .then(response => {
        const { giaodich } = response.data
        callback(giaodich)
        store.state.showBlock = false
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching Data list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  const fetchUsers = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-user/fetchUsers', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        type: 'Thành viên',
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const { users, rows } = response.data

        callback(users)
        totalUsers.value = rows
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }
  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  const resolveUserRoleVariant = role => {
    if (role === 'subscriber') return 'primary'
    if (role === 'author') return 'warning'
    if (role === 'maintainer') return 'success'
    if (role === 'editor') return 'info'
    if (role === 'admin') return 'danger'
    return 'primary'
  }

  const resolveUserRoleIcon = role => {
    if (role === 'subscriber') return 'UserIcon'
    if (role === 'author') return 'SettingsIcon'
    if (role === 'maintainer') return 'DatabaseIcon'
    if (role === 'editor') return 'Edit2Icon'
    if (role === 'admin') return 'ServerIcon'
    return 'UserIcon'
  }

  const resolveUserStatusVariant = user => {
    if (user.con_no <= 0) return 'success'
    return 'danger'
  }

  const xoaKhachHang = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-user/deleteKhachHang', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        khach_hang: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  return {
    fieldsTimKiem,
    fetchUsers,
    xoaKhachHang,
    tableColumns,
    tableDonHangDaDatColumns,
    tableGiaoDichColumns,
    getDonHang,
    getGiaoDich,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,

    resolveUserRoleVariant,
    resolveUserRoleIcon,
    resolveUserStatusVariant,
    refetchData,

    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
