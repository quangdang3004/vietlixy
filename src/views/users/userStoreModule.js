import axios from '@axios'
import config from '../../config.json'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    taiFile(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/xuat-file-excel`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    chuyenTrangThai(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}don-hang/thay-doi-trang-thai`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchUsers(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-data`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    xemChiTiet(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/xem-chi-tiet`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchAllKhuVuc(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}danh-muc/get-all-khu-vuc`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchAllKinhDoanh(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-all-kinh-doanh`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchAllDaiLy(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-all-dai-ly`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchAllDaiLyList(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-all-dai-ly-list`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getListVaiTro(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}vai-tro/get-all-vai-tro`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    addKhachHang(ctx, userData) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/save`, userData, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getKhachHang(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/load`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    loadThongTinThanhToan(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/load-thong-tin-thanh-toan`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    thanhToan(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/khach-hang-thanh-toan`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    rutTien(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/khach-hang-rut-tien`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    napTien(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}giao-dich/dai-ly-nap-tien`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    nhanMaGiaoDich(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}giao-dich/sendmagiaodich`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getTree(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-tree`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    loadProfile(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/load-profile`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    loadAffiliate(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/load-affiliate`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    updateProfile(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/update-profile`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    deleteKhachHang(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/delete`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getDonHang(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-data`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getDonKyGui(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-don-ky-gui`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    getGiaoDich(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}user/get-giao-dich`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
