import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useChienDichList() {
  // Use toast
  const toast = useToast()
  const refChienDichListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    {
      key: 'selected', label: 'Chọn', sortable: false, class: 'width-1',
    },
    { key: 'id', label: 'STT', thStyle: { width: '1%' } },
    { key: 'image', label: 'Image' },
    { key: 'title', label: 'Tên chiến dịch' },
    { key: 'hoa_hong_tu_net', label: 'Hoa hồng' },
    { key: 'network', label: 'Network' },
    { key: 'noi_bat', label: 'Nổi bật' },
    { key: 'vi_tri_hien_thi', label: 'Vị trí' },
    { key: 'trang_thai', label: 'Trạng thái' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPage = ref(50)
  const totalChienDichs = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const fieldsTimKiem = ref([])

  const dataMeta = computed(() => {
    const localItemsCount = refChienDichListTable.value ? refChienDichListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalChienDichs.value,
    }
  })

  const refetchData = () => {
    refChienDichListTable.value.refresh()
  }

  const currentUser = getUserData()

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
  const fetchChienDichs = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-chien-dich/fetchData', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const {
          results,
          rows,
        } = response.data

        callback(results)
        totalChienDichs.value = rows
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }
  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  const xoaChienDich = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-chien-dich/delete', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        chienDich: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  const xoaNhieuChienDich = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-chien-dich/deleteChienDichs', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        chienDichs: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  return {
    fieldsTimKiem,
    fetchChienDichs,
    xoaChienDich,
    xoaNhieuChienDich,
    tableColumns,
    perPage,
    currentPage,
    totalChienDichs,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refChienDichListTable,

    refetchData,

    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
