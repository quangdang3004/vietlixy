import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import axios from 'axios'
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useKhuVucList() {
  // Use toast
  const toast = useToast()
  const refUserListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', label: 'STT', thStyle: { width: '1%' } },
    { key: 'image', sortable: true, label: 'Logo' },
    { key: 'name', sortable: true, label: 'Tên' },
    { key: 'type', sortable: true, label: 'Nhóm' },
    { key: 'ti_le_hoan_tien', sortable: true, label: 'Tỉ lệ hoàn tiền' },
    { key: 'type_hoan_tien', sortable: true, label: 'Loại hoàn tiền' },
    { key: 'link', sortable: true, label: 'Facebook/Zalo' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const fieldsTimKiem = ref([])

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }

  const currentUser = getUserData()

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
  const fetchUsers = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-khu-vuc/fetchGetDanhMuc', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const { results, rows } = response.data

        callback(results)
        totalUsers.value = rows
      })
      .catch(e => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Thông báo',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
            text: e.response,
          },
        })
      })
  }
  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  const xoaKhachHang = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-khu-vuc/delete', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        danh_muc: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  return {
    fieldsTimKiem,
    fetchUsers,
    xoaKhachHang,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,
    refetchData,
    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
