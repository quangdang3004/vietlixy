import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useThongBaoList() {
  // Use toast
  const toast = useToast()
  const refThongBaoListTable = ref(null)
  const fieldsTimKiem = ref([])

  // Table Handlers
  const tableColumns = [
    { key: 'active', label: 'STT', thStyle: { width: '1%' } },
    { key: 'ma_khoan_vay', label: 'Mã khoản vay' },
    { key: 'ho_ten', label: 'Khách Hàng' },
    { key: 'lai_toi_hom_nay', label: 'Lãi tới hôm nay' },
  ]
  const perPage = ref(10)
  const totalThongBaos = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)

  const dataMeta = computed(() => {
    const localItemsCount = refThongBaoListTable.value ? refThongBaoListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalThongBaos.value,
    }
  })

  const refetchData = () => {
    refThongBaoListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
  const fetchThongBaos = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-thong-bao/fetchData', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const {
          results, rows,
        } = response.data

        callback(results)
        totalThongBaos.value = rows
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  return {
    fieldsTimKiem,
    fetchThongBaos,
    tableColumns,
    perPage,
    currentPage,
    totalThongBaos,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refThongBaoListTable,

    refetchData,

    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
