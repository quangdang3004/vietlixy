import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import axios from 'axios'
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'
import router from '@/router'

export default function useGiaoDichList() {
  // Use toast
  const toast = useToast()
  const refUserListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    {
      key: 'selected', label: 'Chọn', sortable: false, class: 'width-1',
    },
    { key: 'index', label: 'STT', thStyle: { width: '1%' } },
    { key: 'username', sortable: true, label: 'Người thực hiện' },
    { key: 'cost', sortable: true, label: 'Số tiền' },
    { key: 'created', sortable: true, label: 'Ngày giao dịch' },
    { key: 'type', sortable: true, label: 'Loại giao dịch' },
    { key: 'bank_number', sortable: true, label: 'Số tài khoản' },
    { key: 'bank_name', sortable: true, label: 'Tên ngân hàng' },
    { key: 'trang_thai', sortable: true, label: 'Trạng thái' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPage = ref(20)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const tongTienData = ref(null)
  const fieldsTimKiem = ref([])

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
  const currentUser = getUserData()
  const xacNhanRutTien = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-giao-dich/xacNhanRutTien', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        giaoDichID: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        callback()
        after()
        toast({
          component: ToastificationContent,
          props: {
            title: 'Thông báo',
            icon: 'BellIcon',
            variant: 'danger',
            text: e.response.data.message,
          },
        })
      })
  }
  const fetchUsers = (ctx, callback) => {
    const userData = getUserData()
    store.state.showBlock = true
    store
      .dispatch('app-giao-dich/fetchGiaoDich', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
        type: router.currentRoute.params.type,
      })
      .then(response => {
        const { results, tongTien, rows } = response.data

        callback(results)
        totalUsers.value = rows
        tongTienData.value = tongTien
        store.state.showBlock = false
      })
      .catch(e => {
        store.state.showBlock = false
        toast({
          component: ToastificationContent,
          props: {
            title: 'Thông báo',
            icon: 'BellIcon',
            variant: 'danger',
            text: e.response.data.message,
          },
        })
      })
  }
  return {
    fieldsTimKiem,
    fetchUsers,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,
    refetchData,
    xacNhanRutTien,
    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
    tongTienData,
  }
}
