import axios from '@axios'
import config from '../../config.json'

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchGiaoDich(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}giao-dich/get-data`, queryParams, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    xacNhanRutTien(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}giao-dich/confirm`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    xemChiTiet(ctx, params) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${config.apiUrl.mainUrl}giao-dich/xem-chi-tiet`, params, {
            headers: {
              'Content-Type': 'multipart/json',
            },
          })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
  },
}
